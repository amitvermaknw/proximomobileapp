import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { BottomMenuModule } from "../shared/ui/bottom-menu/bottom-menu.module";

import { BatchListRoutingModule } from "./batch-list-routing.module";
import { BatchListComponent } from './batch-list.component';
import { ActionBarModule } from '../shared/ui/action-bar/action-bar.module';


@NgModule({
  declarations: [
      BatchListComponent
  ],
  imports: [
    NativeScriptCommonModule,
    NativeScriptUIListViewModule,
    BatchListRoutingModule,
    ActionBarModule,
    BottomMenuModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class BatchListModule { }
