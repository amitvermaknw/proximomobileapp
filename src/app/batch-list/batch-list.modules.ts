export class BatchListModules {

    batchId: number;
    batchName: string;
    totalStudent: number;
    trainingCenter: string;
    trainingPartner?: string;
    createdOn?: string;
    testName: string;
    Status?: number;
    Message?: [];
    constructor(batchList) {
        this.batchId = batchList.batchId;
        this.batchName = batchList.batchName;
        this.totalStudent = batchList.totalStudent;
        this.trainingCenter = batchList.trainingCenter;
        this.trainingPartner = batchList.trainingPartner;
        this.createdOn = batchList.createdOn;
        this.testName = batchList.testName;
        this.Status = batchList.Status;
        this.Message = batchList.Message;
    }
}
