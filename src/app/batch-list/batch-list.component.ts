import { Component, OnInit } from "@angular/core";
import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";
import { knownFolders, File, Folder } from "tns-core-modules/file-system";
import { setString } from "tns-core-modules/application-settings";

import { PageRoute, RouterExtensions } from "nativescript-angular";
import { ApiService } from "../shared/services/api.service";
import { UIServices } from "../shared/ui/ui.services";
import { BatchListModules } from "./batch-list.modules";

@Component({
    moduleId: module.id,
    selector: "ns-batch-list",
    templateUrl: "./batch-list.component.html",
    providers: [ApiService],
    styleUrls: ["./batch-list.component.android.scss"]
})
export class BatchListComponent implements OnInit {

    get dataItems(): ObservableArray<BatchListModules> {
        return this._dataItems;
    }
    batchList: BatchListModules;
    categoriesIcons: {};
    documents = knownFolders.documents();
    folder: Folder;
    file: File;
    processing: boolean;

    private _dataItems: ObservableArray<BatchListModules>;

    constructor(
        private _router: RouterExtensions,
        private _pageRoute: PageRoute,
        private _apiService: ApiService,
        private _uiServices: UIServices) {
            this.categoriesIcons = this._uiServices.getCategoriesIcons();

        }

    ngOnInit() {
        this.processing = true;
        console.log("indide batchlist");
        this._pageRoute.activatedRoute.subscribe((activatedRoute) => {
            activatedRoute.paramMap.subscribe((paramMap) => {
                const userId = +paramMap.get("id");
                if (userId) {
                    this._apiService.fetchBatchList(userId).subscribe(
                        (res: any) => {
                            if (res[0].Status === "200") {
                                this._dataItems = new ObservableArray(res[0].Message);
                                this.createBatchList("proximo", "batchlist", JSON.stringify(res));
                            } else {
                                console.log("There is some issue. Try after some time");
                            }
                        },
                        (err) => {
                            console.log("Error while calling api", err);
                        }
                    );
                }
            });

        });
    }

    getBadgeIcon(id: string) {
        return this.categoriesIcons[id];
    }

    onBatchListTap(args: any) {
        const batchId = args.view.bindingContext.BatchId;
        setString("batchid", batchId);
        this._router.navigate([`/downloadfile`]);

        // this._router.navigate([`/batchevidence/${batchId}`]);
    }

    createBatchList(folderName, fileName, fileContent) {
        this.folder = this.documents.getFolder(folderName || "proximo");
        this.file = this.folder.getFile((fileName || "batchlist") + ".json");

        this.file.writeText(fileContent).then((result) => {
            console.log("Batchlist written successfully");
            this.processing = false;
        }).catch((err) => {
            console.log(err);
        });
    }

}
