import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NSEmptyOutletComponent } from "nativescript-angular";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { AuthComponent } from "./auth/auth.component";

const routes: Routes = [
    // {
    //     path: "",
    //     redirectTo: "/(homeTab:home/default//browseTab:browse/default//searchTab:search/default)",
    //     pathMatch: "full"
    // },
    {
        path: "",
        component: AuthComponent
        // loadChildren: "~/app/auth/auth.module#AuthModule"
    },
    // {
    //     path: "home",
    //     component: NSEmptyOutletComponent,
    //     loadChildren: "~/app/home/home.module#HomeModule"
    //     // outlet: "homeTab"
    // },
    // {
    //     path: "batchlist",
    //     component: NSEmptyOutletComponent,
    //     loadChildren: "~/app/batch-list/batch-list.module#BatchListModule",
    //     //outlet: "batchlist"
    // },
    // {
    //     path: "batchevidence",
    //     component: NSEmptyOutletComponent,
    //     loadChildren: "~/app/batch-evidence/batch-evidence.module#BatchEvidenceModule",
    //     //outlet: "batchevidence"
    // },
    {
        path: "batchlist",
        loadChildren: "./batch-list/batch-list.module#BatchListModule"
    },
    {
        path: "batchevidence",
        loadChildren: "./batch-evidence/batch-evidence.module#BatchEvidenceModule"
    },
    {
        path: "exam",
        loadChildren: "./exam/exam.module#ExamModule"
    },
    {
        path: "setting",
        loadChildren: "./setting/setting.module#SettingModule"
    },
    {
        path: "downloadfile",
        loadChildren: "./downloadfile/downloadfile.module#DownloadfileModule"
    },
    {
        path: "browse",
        component: NSEmptyOutletComponent,
        loadChildren: "~/app/browse/browse.module#BrowseModule",
        outlet: "browseTab"
    },
    {
        path: "search",
        component: NSEmptyOutletComponent,
        loadChildren: "~/app/search/search.module#SearchModule",
        outlet: "searchTab"
    }
];

// const routes: Routes = []

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
