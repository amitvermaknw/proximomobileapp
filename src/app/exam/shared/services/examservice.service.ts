import { Injectable } from '@angular/core';
import { ImageSource } from "tns-core-modules/image-source/image-source";
import * as fs from "tns-core-modules/file-system";
import { CameraService } from "../../../batch-evidence/shared/services/camera.service";
import { ModalDialogOptions, ModalDialogService } from "nativescript-angular/modal-dialog";
import { StudentPracticalExRecord } from "../../exam.modules";

@Injectable({
    providedIn: 'root'
})
export class ExamserviceService {

    constructor(
        private camera: CameraService,
        private modal: ModalDialogService
    ) { }

    savePracticalStdPhotos(studentPracticalExRecord: StudentPracticalExRecord) {

        return new Promise((resolve) => {
            const source = new ImageSource();
            this.camera.takePhoto()
                .then((imageAsset) => {

                    source.fromAsset(imageAsset).then((imageSource: ImageSource) => {

                        if (fs.knownFolders.documents().contains("proximo")) {
                            // tslint:disable-next-line: max-line-length
                            const practicalImgFolderPath = fs.path.join(fs.knownFolders.documents().getFolder("proximo").path, "PracticalStdImages");
                            const practicalImgFolder = fs.Folder.fromPath(practicalImgFolderPath);

                            // tslint:disable-next-line: max-line-length
                            const fileName = `${studentPracticalExRecord.studentName}${Math.round(new Date().getTime() / 1000)}.jpg`;
                            const filePath = fs.path.join(practicalImgFolder.path, fileName);
                            const saved: boolean = imageSource.saveToFile(filePath, "jpg");
                            const imgJSONFilePath = fs.path.join(practicalImgFolderPath, "savedPracticalImgFile.json");
                            const imageJSONExist: boolean = fs.File.exists(imgJSONFilePath);

                            const imageJSONContent = {
                                type: studentPracticalExRecord.type,
                                path: filePath,
                                date: new Date("MM/DD/YYYY"),
                                name: fileName,
                                studentName: studentPracticalExRecord.studentName,
                                studentBatchId: studentPracticalExRecord.studentBatchId,
                                studentId: studentPracticalExRecord.studentId,
                                photoType: studentPracticalExRecord.photoType
                            };

                            let imageJSONFile;

                            if (!imageJSONExist) {
                                imageJSONFile = practicalImgFolder.getFile("savedPracticalImgFile.json");

                                const content = [];
                                content.push(imageJSONContent);

                                const imageDetail = JSON.stringify(content);

                                imageJSONFile.writeText(imageDetail).then((result) => {
                                    console.log("Image detail is added first time in json file");

                                    imageJSONFile.readText().then((jsonContent: any) => {
                                        return resolve(jsonContent);
                                    });

                                }).catch((err) => {
                                    console.log(err);

                                    return resolve(false);
                                });
                            } else {

                                imageJSONFile = practicalImgFolder.getFile("savedPracticalImgFile.json");

                                imageJSONFile.readText().then((existingContent: any) => {

                                    if (existingContent) {
                                        let addedNewContentArr = [];
                                        addedNewContentArr = JSON.parse(existingContent);
                                        addedNewContentArr.push(imageJSONContent);

                                        const addedNewContent = JSON.stringify(addedNewContentArr);

                                        imageJSONFile.writeText(addedNewContent).then((result) => {
                                            // console.log("Image detail is updated in json file");

                                            imageJSONFile.readText().then((jsonContent: any) => {
                                                return resolve(jsonContent);
                                            });

                                        }).catch((err) => {
                                            console.log(err);

                                            return resolve(false);
                                        });
                                    }

                                }).catch((err) => {
                                    console.log(err.stack);

                                    return resolve(false);
                                });

                            }

                            if (saved) {
                                console.log("Image saved");
                            }
                        }
                    });

                    /*setTimeout(() => {
                        this.modal.showModal(FilterComponent, options).then((response) => {
                            if (response == 'success') {
                                // this.onNavtap('profile', '4');
                            }
                            else {
                                // this.onNavtap('home', '0');
                            }
                        }, error => {
                            console.log(error);
                        });
                    }, 1000);*/

                }).catch((err) => {
                    console.log(err.message);

                    return resolve(false);
                });
        });
    }

    fetchPhotoFile() {

        return new Promise((resolve) => {
            if (fs.knownFolders.documents().contains("proximo")) {
                // tslint:disable-next-line: max-line-length
                const assessorImgFolderPath = fs.path.join(fs.knownFolders.documents().getFolder("proximo").path, "PracticalStdImages");
                const assessorImgFolder = fs.Folder.fromPath(assessorImgFolderPath);
                const imgJSONFilePath = fs.path.join(assessorImgFolderPath, "savedPracticalImgFile.json");
                const imageJSONExist: boolean = fs.File.exists(imgJSONFilePath);

                if (imageJSONExist) {
                    const imageJSONFile = assessorImgFolder.getFile("savedPracticalImgFile.json");

                    imageJSONFile.readText().then((existingContent: any) => {
                        if (existingContent) {
                            return resolve(existingContent);
                        } else {
                            return resolve(false);
                        }

                    }).catch((err) => {
                        console.log(err.stack);

                        return resolve(false);
                    });
                } else {
                    return resolve(false);
                }

            } else {
                return resolve(false);
            }
        });

    }

    savePracticalVideos(studentPracticalExRecord: StudentPracticalExRecord) {

        return new Promise((resolve) => {
            this.camera.takeVideo()
                .then((videoPath: any) => {

                    if (videoPath) {

                        if (fs.knownFolders.documents().contains("proximo")) {
                            // tslint:disable-next-line: max-line-length
                            const assessorImgFolderPath = fs.path.join(fs.knownFolders.documents().getFolder("proximo").path, "PracticalStdImages");
                            const assessorImgFolder = fs.Folder.fromPath(assessorImgFolderPath);

                            const filePath = fs.path.join(assessorImgFolder.path, videoPath);
                            // const saved: boolean = imageSource.saveToFile(filePath, "jpg");
                            const imgJSONFilePath = fs.path.join(assessorImgFolderPath, "savedPracticalImgFile.json");
                            const imageJSONExist: boolean = fs.File.exists(imgJSONFilePath);

                            const imageJSONContent = {
                                type: studentPracticalExRecord.type,
                                path: videoPath,
                                date: new Date("MM/DD/YYYY"),
                                name: videoPath,
                                studentName: studentPracticalExRecord.studentName,
                                studentBatchId: studentPracticalExRecord.studentBatchId,
                                studentId: studentPracticalExRecord.studentId,
                                videoType: studentPracticalExRecord.videoType
                            };

                            let imageJSONFile;

                            if (!imageJSONExist) {
                                imageJSONFile = assessorImgFolder.getFile("savedPracticalImgFile.json");

                                const content = [];
                                content.push(imageJSONContent);

                                const imageDetail = JSON.stringify(content);

                                // console.log("Content = " + imageDetail);
                                imageJSONFile.writeText(imageDetail).then((result) => {
                                    console.log("Image detail is added first time in json file");

                                    imageJSONFile.readText().then((jsonContent: any) => {
                                        return resolve(jsonContent);
                                    });

                                }).catch((err) => {
                                    console.log(err);

                                    return resolve(false);
                                });
                            } else {

                                imageJSONFile = assessorImgFolder.getFile("savedPracticalImgFile.json");

                                imageJSONFile.readText().then((existingContent: any) => {

                                    if (existingContent) {
                                        let addedNewContentArr = [];
                                        addedNewContentArr = JSON.parse(existingContent);
                                        addedNewContentArr.push(imageJSONContent);

                                        const addedNewContent = JSON.stringify(addedNewContentArr);

                                        imageJSONFile.writeText(addedNewContent).then((result) => {
                                            // console.log("Image detail is updated in json file");

                                            imageJSONFile.readText().then((jsonContent: any) => {
                                                return resolve(jsonContent);
                                            });

                                        }).catch((err) => {
                                            console.log(err);

                                            return resolve(false);
                                        });
                                    }

                                }).catch((err) => {
                                    console.log(err.stack);

                                    return resolve(false);
                                });

                            }
                        }
                    } else {
                        console.log("Video Saved");
                    }

                }).catch((err) => {
                    console.log(err.message);

                    return resolve(false);
                });
        });
    }

    fetchVideoFile() {

        return new Promise((resolve) => {
            if (fs.knownFolders.documents().contains("proximo")) {
                // tslint:disable-next-line: max-line-length
                const assessorImgFolderPath = fs.path.join(fs.knownFolders.documents().getFolder("proximo").path, "PracticalStdImages");
                const assessorImgFolder = fs.Folder.fromPath(assessorImgFolderPath);
                const imgJSONFilePath = fs.path.join(assessorImgFolderPath, "savedPracticalImgFile.json");
                const imageJSONExist: boolean = fs.File.exists(imgJSONFilePath);

                if (imageJSONExist) {
                    const imageJSONFile = assessorImgFolder.getFile("savedPracticalImgFile.json");

                    imageJSONFile.readText().then((existingContent: any) => {
                        if (existingContent) {
                            return resolve(existingContent);
                        } else {
                            return resolve(false);
                        }

                    }).catch((err) => {
                        console.log(err.stack);

                        return resolve(false);
                    });
                } else {
                    return resolve(false);
                }

            } else {
                return resolve(false);
            }
        });

    }
}
