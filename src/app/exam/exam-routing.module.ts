import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { NativeScriptRouterModule } from 'nativescript-angular';
import { Routes } from "@angular/router";

import { ExamComponent } from "./exam.component";
import { PreExamComponent } from "./pre-exam/pre-exam.component";
import { StudentListComponent } from "./theory-exam/student-list/student-list.component";
import { ExamGridComponent } from "./theory-exam/exam-grid/exam-grid.component";
import { PracStudentListComponent } from "./practical-exam/prac-student-list/prac-student-list.component";
import { PracExamSheetComponent } from "./practical-exam/prac-exam-sheet/prac-exam-sheet.component";

const routes: Routes = [
    { path: "", component: ExamComponent },
    { path: "preexam", component: PreExamComponent },
    { path: "theorystudentlist/:batchid", component: StudentListComponent},
    { path: "examgrid/:studentId", component: ExamGridComponent},
    { path: "practicalstudentlist/:batchid", component: PracStudentListComponent},
    { path: "practicalexam/:studentId", component: PracExamSheetComponent}
];

@NgModule({
  declarations: [
  ],
  imports: [
    NativeScriptCommonModule,
    NativeScriptRouterModule.forChild(routes)
  ],
  exports: [NativeScriptRouterModule],
  schemas: [NO_ERRORS_SCHEMA]
})
export class ExamRoutingModule { }
