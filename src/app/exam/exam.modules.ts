export class StudentPracticalExRecord {
    type: string;
    studentId: number;
    studentName: string;
    studentBatchId: number;
    photoType: string;
    videoType: string
    constructor(batchList) {
        this.type = batchList.type;
        this.studentId = batchList.studentId;
        this.studentName = batchList.studentName;
        this.studentBatchId = batchList.studentBatchId;
        this.photoType = batchList.photoType;
        this.videoType = batchList.videoType;
    }
}
