import { Component, OnInit, Pipe, PipeTransform, ViewChild, ElementRef } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular";
import { RadioOption } from "./radio-option";

import { timer } from "rxjs";
import { map } from "rxjs/operators";
import { UIServices } from "~/app/shared/ui/ui.services";

@Component({
    moduleId: module.id,
    selector: "ns-question-sheet",
    templateUrl: "./question-sheet.component.html",
    styleUrls: ["./question-sheet.component.android.scss"]
})
export class QuestionSheetComponent implements OnInit {
    @ViewChild('elem', { static: false }) ansCheckBox: ElementRef;

    processing: boolean = true;
    studentInfo: any;
    testNameAndTime: { testName: string, testTime: number };
    checkTest: boolean;
    radioOptions?: Array<RadioOption> = [];
    questionText: string;
    queParsedParams: any;
    queNumber: number = 0;
    nextBtnVisibility: boolean = false;
    backBtnVisibility: boolean = false;
    questionNumber: number;
    nextBtnClass: string;
    backBtnClass: string;
    totalQuestion: number;
    questionPosition: number = 0;
    stdAnswer: string = "";

    countDown;
    tick = 1000;

    constructor(
        private modalDialogParams: ModalDialogParams,
        private _uiService: UIServices
    ) { }

    ngOnInit() {
        this.setQueOnPage();
    }

    setQueOnPage() {
        this.queParsedParams = this.modalDialogParams.context as {
            queObj: any;
            queId: string;
            gId: string;
            studentInfo: any;
            testNameAndTime: { testName: string, testTime: number }
        };

        this.studentInfo = this.queParsedParams.studentInfo[0];
        this.studentInfo.queRecord = [];
        this.testNameAndTime = this.queParsedParams.testNameAndTime;

        const questionObject = this.queParsedParams.queObj.filter((que: any) => {
            if (que.TheoryQuestionId === this.queParsedParams.queId) {
                return que;
            }
        });

        this.queNumber = + this.queParsedParams.queId;
        this.nextBtnVisibility = true;
        this.totalQuestion = + this.queParsedParams.queObj.length;
        this.questionPosition = + this.queParsedParams.gId;

        this.questionNumber = questionObject[0].Id;
        this.setQueOptOnNext(this.questionNumber - 1);
        this.setQuestion(questionObject);

        this.nextBtnClass = "btn btn-info btn-rounded-sm";
        this.backBtnClass = "btn btn-default btn-rounded-sm";

        this.setBackandNextBtnVisibility();
        this.startTimer();
        this.processing = false;
    }

    nextQuestion(queAskedForStd: string) {
        this.saveQueandAns().then(() => {
            this.queNumber = this.queNumber + 1;
            const questionObject = this.queParsedParams.queObj.filter((que: any) => {
                if (+que.TheoryQuestionId === this.queNumber) {
                    return que;
                }
            });

            if (questionObject.length) {
                this.setQuestion(questionObject);
            }

            this.setQueOptOnNext(this.questionNumber);
            this.questionNumber = questionObject[0].Id;
            this.questionPosition = this.questionPosition + 1;
            this.setBackandNextBtnVisibility();
        });
    }

    backQuestion() {

        this.queNumber = this.queNumber - 1;
        const questionObject = this.queParsedParams.queObj.filter((que: any) => {
            if (+que.TheoryQuestionId === this.queNumber) {
                return que;
            }
        });

        if (questionObject.length) {
            this.setQuestion(questionObject);
        }
        this.setQueOptOnBack(this.questionNumber);
        this.questionNumber = questionObject[0].Id;
        this.questionPosition = this.questionPosition - 1;
        this.setBackandNextBtnVisibility();

        // this.saveQueandAns();
    }

    setQuestion(questionParam) {

        if (questionParam.length) {

            this.radioOptions = [];
            questionParam.map((que) => {
                this.questionText = que.QuestionText;
                if (que.OptionA) {
                    this.radioOptions.push(new RadioOption(que.OptionA));
                }
                if (que.OptionB) {
                    this.radioOptions.push(new RadioOption(que.OptionB));
                }
                if (que.OptionC) {
                    this.radioOptions.push(new RadioOption(que.OptionC));
                }
                if (que.OptionD) {
                    this.radioOptions.push(new RadioOption(que.OptionD));
                }
                if (que.OptionE) {
                    this.radioOptions.push(new RadioOption(que.OptionE));
                }
            });
        }
    }

    setBackandNextBtnVisibility() {

        if (this.questionPosition === 1 || this.questionNumber === 1) {
            this.backBtnVisibility = false;
            this.backBtnClass = "btn btn-default-disabled btn-rounded-sm";
        } else {
            this.backBtnClass = "btn btn-default btn-rounded-sm";
            this.backBtnVisibility = true;
        }

        if (this.queParsedParams.queObj.length === this.queParsedParams.gId
            || this.questionNumber === this.queParsedParams.queObj.length) {
            this.nextBtnVisibility = false;
            this.nextBtnClass = "btn btn-info-disabled btn-rounded-sm";
        } else {
            this.nextBtnClass = "btn btn-info btn-rounded-sm";
            this.nextBtnVisibility = true;
        }
    }

    changeCheckedRadio(radioOption: RadioOption): void {
        this.stdAnswer = "";
        radioOption.selected = !radioOption.selected;

        if (!radioOption.selected) {
            return;
        }

        // uncheck all other options
        this.radioOptions.forEach((option) => {
            if (option.text !== radioOption.text) {
                option.selected = false;
            } else {
                option.selected = true;
            }
        });

        this.stdAnswer = radioOption.text;
    }

    checkedChange(modelRef) {
        console.log("checkedChange:", modelRef.checked);
    }

    closeModal() {
        this.modalDialogParams.closeCallback();
    }

    startTimer() {
        this.countDown = timer(0, this.tick)
            .pipe(map(() => --this.testNameAndTime.testTime));
    }

    saveQueandAns() {
        return new Promise((resolve) => {
            const stdQueSheet = {
                queId: this.questionNumber,
                que: this.questionText,
                ans: this.stdAnswer
            };

            this._uiService.addStdExamQue(stdQueSheet, this.studentInfo).then(() => {
                return resolve();
            });
        });
    }

    setQueOptOnNext(queNumber: number) {
        this._uiService.readStdExamQue(queNumber + 1).then((res: any) => {
            if (res) {
                res.selected = false;
                res.text = res.ans;
                this.changeCheckedRadio(res);
            }
        });
    }

    setQueOptOnBack(queNumber: number) {
        this._uiService.readStdExamQue(queNumber - 1).then((res: any) => {
            if (res) {
                res.selected = false;
                res.text = res.ans;
                this.changeCheckedRadio(res);
            }
        });
    }

}

// tslint:disable-next-line: max-classes-per-file
@Pipe({
    name: "formatTime"
})
export class FormatTimePipe implements PipeTransform {

    transform(value: number): string {
        const hours: number = Math.floor(value / 3600);
        const minutes: number = Math.floor((value % 3600) / 60);
        return ("00" + hours).slice(-2) + ":" + ("00" + minutes).slice(-2) + ":" + ("00" + Math.floor(value - minutes * 60)).slice(-2);
    }

}
