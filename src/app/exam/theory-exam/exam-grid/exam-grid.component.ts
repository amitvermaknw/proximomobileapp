import { Component, OnInit, ViewContainerRef } from "@angular/core";
import { PageRoute, RouterExtensions, ModalDialogService } from "nativescript-angular";
import { getString } from "tns-core-modules/application-settings";
import { UIServices } from "~/app/shared/ui/ui.services";
import { QuestionSheetComponent } from "./question-sheet/question-sheet.component";
import { alert } from "tns-core-modules/ui/dialogs";

@Component({
    moduleId: module.id,
    selector: 'ns-exam-grid',
    templateUrl: './exam-grid.component.html',
    styleUrls: ['./exam-grid.component.android.scss']
})
export class ExamGridComponent implements OnInit {

    processing: boolean = true;
    questionsListObj: Array<any> = [];
    coumnNumber: number = 0;
    studentInfo: any;
    testNameAndTime: { testName: string, testTime: number };

    constructor(
        private _pageRoute: PageRoute,
        private _router: RouterExtensions,
        private _uiService: UIServices,
        private modalDialog: ModalDialogService,
        private vcRef: ViewContainerRef
    ) { }

    ngOnInit() {

        console.log("Inside Exam grid");
        this._pageRoute.activatedRoute.subscribe((activatedRoute) => {
            activatedRoute.paramMap.subscribe((paramMap) => {
                const studentId = +paramMap.get("studentId");

                if (studentId) {

                    this._uiService.readFiles("proximo", "studentlist.json").then((result: any) => {

                        if (result.code === 200) {

                            const questionListData = JSON.parse(result.data);
                            if (questionListData.BatchData.length) {

                                /** Get student information */
                                this.studentInfo = questionListData.BatchData.filter((res) => {
                                    if (studentId === +res.StudentUserIdId) {
                                        return res;
                                    }
                                });

                                /** Get question object */
                                if (questionListData.QuestionData.length) {

                                    const queAssending = questionListData.QuestionData.sort((a, b) => {
                                        const x = (+a.TheoryQuestionId - +b.TheoryQuestionId);

                                        return x === 0 ? a.TheoryQuestionId - b.TheoryQuestionId : x;
                                    });

                                    /** Add Id to detect question number */
                                    let id = 1;
                                    queAssending.forEach((data) => {
                                        data.Id = id;
                                        id = id + 1;

                                        return data;
                                    });
                                    this.questionsListObj = queAssending;
                                }

                                this.testNameAndTime = {
                                    testName: questionListData.TestName,
                                    testTime: questionListData.TestTimeDuration
                                };

                                this.processing = false;
                            }
                        } else {
                            alert({
                                title: "Alert",
                                message: "There was some error while submitting the exam. Please try after sometime.",
                                okButtonText: "Okay"
                            }).then(() => {
                                console.log("The user closed the alert.");
                            });
                        }
                    })
                    .catch((error) => {
                        console.log("error while reading studentlist");
                        alert({
                            title: "Alert",
                            message: "There was some error while submitting the exam. Please try after sometime.",
                            okButtonText: "Okay"
                        }).then(() => {
                            console.log("The user closed the alert.");
                        });
                    });
                }
            });
        });
    }

    getRow(index: number, queLen) {
        const startRow = 1;
        const weekRow = Math.floor(index / 7);
        return startRow + weekRow;
    }

    getColumn(index: number, queLen) {
        const columnCount = index % 7;
        if (columnCount === 0) {
            this.coumnNumber = 0;
            return this.coumnNumber;
        } else {
            this.coumnNumber++;
            return this.coumnNumber;
        }
    }

    submitExam() {
        this.processing = true;
        const date = new Date();
        // tslint:disable-next-line: max-line-length
        const stdFileName = `thy_${this.studentInfo[0].StudentUserIdId}_${this.studentInfo[0].StudentName}_${date.getTime()}`;
        this._uiService.saveStdExamQue(stdFileName).then((res: any) => {
            if (res.code === 200) {
                alert({
                    title: "Success",
                    message: "Your exam submitted successfully.",
                    okButtonText: "Okay"
                }).then(() => {

                    console.log("Closed popup after submit exam.");
                    const batchObj = JSON.parse(getString("batchlistobj"));
                    this._router.navigate([`/exam/theorystudentlist/${batchObj.BatchId}`]);
                });

            } else {
                alert({
                    title: "Alert",
                    message: "There was some error while submitting the exam. Please try after sometime.",
                    okButtonText: "Okay"
                }).then(() => {
                    console.log("The user closed the alert.");
                });
            }

            this.processing = false;
        });
    }

    onQuestionIndex(questionId, gridId) {

        this.modalDialog
            .showModal(QuestionSheetComponent, {
                fullscreen: true,
                viewContainerRef: this.vcRef,
                context: {
                    queObj: this.questionsListObj,
                    queId: questionId,
                    gId: gridId,
                    studentInfo: this.studentInfo,
                    testNameAndTime: this.testNameAndTime
                }
            })
            .then((action: string) => {
                console.log(action);
            });
    }

}
