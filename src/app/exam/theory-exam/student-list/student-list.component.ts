import { Component, OnInit } from '@angular/core';
import { ApiService } from '~/app/shared/services/api.service';
import { PageRoute, RouterExtensions } from 'nativescript-angular';
import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";
import { knownFolders, File, Folder } from "tns-core-modules/file-system";
import { UIServices } from '~/app/shared/ui/ui.services';

@Component({
    moduleId: module.id,
    selector: 'ns-student-list',
    templateUrl: './student-list.component.html',
    styleUrls: ["./student-list.component.android.scss"]
})
export class StudentListComponent implements OnInit {

    get sudentListObj(): ObservableArray<any> {
        return this._sudentListObj;
    }

    _sudentListObj: ObservableArray<any>;
    documents = knownFolders.documents();
    folder: Folder;
    file: File;
    processing: boolean = true;

    constructor(
        private _apiService: ApiService,
        private _pageRoute: PageRoute,
        private _router: RouterExtensions,
        private _uiService: UIServices
    ) { }

    ngOnInit() {
        this._pageRoute.activatedRoute.subscribe((activatedRoute) => {
            activatedRoute.paramMap.subscribe((paramMap) => {
                const batchId = +paramMap.get("batchid");

                this._uiService.readFiles("proximo", "studentlist.json").then((studentList: any) => {
                    if (studentList.code === 200) {
                        const sList = JSON.parse(studentList.data);
                        this._sudentListObj = new ObservableArray(sList.BatchData);
                    } else {
                        console.log("There is some issue. Try after some time");
                    }
                    this.processing = false;
                });
            });
        });
    }

    createStudentList(folderName, fileName, fileContent) {
        this.folder = this.documents.getFolder(folderName || "proximo");
        this.file = this.folder.getFile((fileName || "studentlist") + ".json");

        this.file.writeText(fileContent).then((result) => {
            console.log("StudentList written successfully");
            this.processing = false;
        }).catch((err) => {
            console.log(err);
        });
    }

    onStudentListTap(args: any) {
        const studentId = args.view.bindingContext.StudentUserIdId;
        this._router.navigate([`/exam/examgrid/${studentId}`]);
    }

}
