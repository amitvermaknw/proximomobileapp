import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { setString } from "tns-core-modules/application-settings";

@Component({
    moduleId: module.id,
    selector: 'ns-pre-exam',
    templateUrl: './pre-exam.component.html',
    styleUrls: ["./pre-exam.component.android.scss"]
})
export class PreExamComponent implements OnInit {

    batchListObj: any;
    processing: boolean = true;

    constructor(
        private _router: Router
    ) {
        this.batchListObj = this._router.getCurrentNavigation().extras.state;
        setString("batchlistobj", JSON.stringify(this.batchListObj));
    }

    ngOnInit() {
        this.processing = false;
    }

    takeTheory() {
        const batchId = this.batchListObj.BatchId;
        this._router.navigate([`/exam/theorystudentlist/${batchId}`]);
    }

    takePractical() {
        const batchId = this.batchListObj.BatchId;
        this._router.navigate([`/exam/practicalstudentlist/${batchId}`]);
    }

}
