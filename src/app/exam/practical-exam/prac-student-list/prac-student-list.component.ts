import { Component, OnInit } from "@angular/core";
import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";
import { knownFolders, File, Folder } from "tns-core-modules/file-system";
import * as _ from "lodash";
import { alert } from "tns-core-modules/ui/dialogs";
import { ApiService } from "~/app/shared/services/api.service";
import { PageRoute, RouterExtensions } from "nativescript-angular";
import { UIServices } from "~/app/shared/ui/ui.services";
import { ExamserviceService } from "../../shared/services/examservice.service";

@Component({
    moduleId: module.id,
    selector: "ns-prac-student-list",
    templateUrl: "./prac-student-list.component.html",
    styleUrls: ["./prac-student-list.component.android.scss"]
})
export class PracStudentListComponent implements OnInit {

    get sudentListObj(): ObservableArray<any> {
        return this._sudentListObj;
    }

    _sudentListObj: ObservableArray<any>;
    processing: boolean = true;
    documents = knownFolders.documents();
    folder: Folder;
    file: File;

    candidatePhotoCSS: string = "cam-layout";
    studentPhotoClicked: boolean = true;
    cssforTheoryVideo1: string = "cam-layout";
    theoryVideo1: boolean = true;
    candidatePhoto: boolean = true;

    constructor(
        private _apiService: ApiService,
        private _pageRoute: PageRoute,
        private _router: RouterExtensions,
        private _uiService: UIServices,
        private _examService: ExamserviceService
    ) { }

    ngOnInit() {
        this._pageRoute.activatedRoute.subscribe((activatedRoute) => {
            activatedRoute.paramMap.subscribe((paramMap) => {
                const batchId = +paramMap.get("batchid");

                this._uiService.readFiles("proximo", "studentlist.json").then((studentList: any) => {
                    if (studentList.code === 200) {
                        const sList = JSON.parse(studentList.data);
                        this._sudentListObj = new ObservableArray(sList.BatchData);
                    } else {
                        console.log("There is some issue. Try after some time");
                    }
                    this.processing = false;
                });

                /*this._apiService.fetchStudentList(batchId).subscribe((studentList: any) => {
                    studentList = JSON.parse(studentList.body);
                    const sList = studentList[0];
                    if (sList.Status === "200") {
                        this._sudentListObj = new ObservableArray(sList.BatchData);

                        this._uiSerice.writeFiles("proximo", "studentlist", JSON.stringify(sList)).then((res) => {
                            console.log("Student list file has been created");
                        });

                    } else {
                        console.log("There is some issue. Try after some time");
                    }
                    this.processing = false;
                });*/
            });
        });
    }

    readPhotoVideos(studentInfo: any) {

        return new Promise((resolve) => {
            this._examService.fetchPhotoFile().then((res: any) => {
                if (res) {
                    const imgDetail = JSON.parse(res);

                    const userFound = _.find(imgDetail, {
                        type: "practicalStd",
                        photoType: studentInfo.photoType,
                        studentId: studentInfo.studentId
                    });

                    const userVideoFound = _.find(imgDetail, {
                        type: "practicalStd",
                        videoType: studentInfo.videoType,
                        studentId: studentInfo.studentId
                    });

                    if (userFound) {
                        alert({
                            title: "Alert",
                            message: "You have already clicked picture for this student.",
                            okButtonText: "Okay"
                        }).then(() => {
                            console.log("The user closed the alert.");
                        });

                        resolve(true);
                    } else if (userVideoFound) {
                        alert({
                            title: "Alert",
                            message: "You have already recorded video for this student.",
                            okButtonText: "Okay"
                        }).then(() => {
                            console.log("The user closed the alert.");
                        });

                        resolve(true);
                    } else {
                        resolve(false);
                    }

                }

                resolve(false);

            }).catch((err) => {
                console.log(err);
            });
        });
    }

    onStudentListTap(args: any) {
        const studentId = args.view.bindingContext.StudentUserIdId;
        console.log(studentId);
        this._router.navigate([`/exam/practicalexam/${studentId}`]);
    }

    takePhoto(type, args: any) {

        const studentInfo = args.view.bindingContext;
        const studentDetail = {
            type: "practicalStd",
            studentId: studentInfo.StudentUserIdId,
            studentName: studentInfo.StudentName,
            studentBatchId: studentInfo.StudentBatchId,
            photoType: type,
            videoType: ""
        };

        this.readPhotoVideos(studentDetail).then((res) => {

            if (!res) {
                this._examService.savePracticalStdPhotos(studentDetail).then((res: any) => {
                    if (res) {
                        const imgDetail = JSON.parse(res);

                        imgDetail.map((data) => {
                            if ((data.type === "practicalStd"
                                && data.photoType === "in"
                                && studentInfo.StudentUserIdId === data.studentId)
                                || (data.type === "practicalStd"
                                    && data.photoType === "adharfront"
                                    && studentInfo.StudentUserIdId === data.studentId)
                                || (data.type === "practicalStd"
                                    && data.photoType === "adharback"
                                    && studentInfo.StudentUserIdId === data.studentId)) {

                                args.view.candidatePhoto = false;
                                args.view.class = "cam-layout-disabled";

                            }
                        });
                    }

                }).catch((err) => {
                    console.log(err);
                });
            } else {
                args.view.candidatePhoto = false;
                args.view.class = "cam-layout-disabled";
            }
        });

    }

    /*studentPhotoClickedStatus(stdId) {
        if (stdId === this.studentPhotoCLicked) {
            return true;
        }
    }*/

    takeVideo(type, args: any) {
        const studentInfo = args.view.bindingContext;
        const studentDetail = {
            type: "practicalStd",
            studentId: studentInfo.StudentUserIdId,
            studentName: studentInfo.StudentName,
            studentBatchId: studentInfo.StudentBatchId,
            photoType: "",
            videoType: type
        };

        this.readPhotoVideos(studentDetail).then((res) => {

            if (!res) {
                this._examService.savePracticalVideos(studentDetail).then((res: any) => {
                    if (res) {
                        const imgDetail = JSON.parse(res);

                        imgDetail.map((data) => {
                            if ((data.type === "practicalStd"
                                && data.videoType === "video"
                                && studentInfo.StudentUserIdId === data.studentId)
                            ) {
                                args.view.theoryVideo1 = false;
                                args.view.class = "cam-layout-disabled";

                            }
                        });
                    }

                }).catch((err) => {
                    console.log(err);
                });
            } else {
                args.view.theoryVideo1 = false;
                args.view.class = "cam-layout-disabled";
            }
        });
    }

}
