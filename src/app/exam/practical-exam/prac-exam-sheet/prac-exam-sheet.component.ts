import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { PageRoute, RouterExtensions } from "nativescript-angular";
import { RadioOption } from "../../theory-exam/exam-grid/question-sheet/radio-option";
import { alert } from "tns-core-modules/ui/dialogs";
import { getString } from "tns-core-modules/application-settings";

import { timer } from "rxjs";
import { map } from "rxjs/operators";
import { UIServices } from "~/app/shared/ui/ui.services";

@Component({
    moduleId: module.id,
    selector: "ns-prac-exam-sheet",
    templateUrl: "./prac-exam-sheet.component.html",
    styleUrls: ["./prac-exam-sheet.component.android.scss"]
})
export class PracExamSheetComponent implements OnInit {

    @ViewChild("elem", { static: false }) ansCheckBox: ElementRef;

    processing: boolean = true;
    studentInfo: any;
    testNameAndTime: { testName: string, testTime: number };
    checkTest: boolean;
    radioOptions?: Array<RadioOption> = [];
    questionText: string;
    queParsedParams: any;
    queNumber: number = 0;
    nextBtnVisibility: boolean = false;
    backBtnVisibility: boolean = false;
    questionNumber: number;
    nextBtnClass: string;
    backBtnClass: string;
    totalQuestion: number;
    questionPosition: number = 0;
    stdAnswer: string = "";

    isSubmitBtnEnabled: boolean = false;

    questionsListObj: Array<any> = [];

    countDown;
    tick = 1000;

    constructor(
        private _uiService: UIServices,
        private _pageRoute: PageRoute,
        private _router: RouterExtensions
    ) { }

    ngOnInit() {
        this._pageRoute.activatedRoute.subscribe((activatedRoute) => {
            activatedRoute.paramMap.subscribe((paramMap) => {
                const studentId = +paramMap.get("studentId");

                if (studentId) {

                    this._uiService.readFiles("proximo", "studentlist.json").then((result: any) => {

                        if (result.code === 200) {

                            const questionListData = JSON.parse(result.data);
                            if (questionListData.BatchData.length) {

                                /** Get student information */
                                this.studentInfo = questionListData.BatchData.filter((res) => {
                                    if (studentId === +res.StudentUserIdId) {
                                        return res;
                                    }
                                });

                                /** Get question object */
                                if (questionListData.QuestionPracticalVivaData.length) {

                                    const queAssending = questionListData.QuestionPracticalVivaData.sort((a, b) => {
                                        const x = (+a.PracticalQuestionId - +b.PracticalQuestionId);

                                        return x === 0 ? a.PracticalQuestionId - b.PracticalQuestionId : x;
                                    });

                                    /** Add Id to detect question number */
                                    let id = 1;
                                    queAssending.forEach((data) => {
                                        data.Id = id;
                                        id = id + 1;

                                        return data;
                                    });
                                    this.questionsListObj = queAssending;
                                }

                                this.testNameAndTime = {
                                    testName: questionListData.TestName,
                                    testTime: questionListData.TestTimeDuration
                                };

                                this.processing = false;
                            }
                        } else {
                            alert({
                                title: "Alert",
                                message: "There was some error while submitting the exam. Please try after sometime.",
                                okButtonText: "Okay"
                            }).then(() => {
                                console.log("The user closed the alert.");
                            });
                        }

                        this.setQueOnPage();
                    })
                        .catch((error) => {
                            console.log("error while reading studentlist");
                            alert({
                                title: "Alert",
                                message: "There was some error while submitting the exam. Please try after sometime." + error.message,
                                okButtonText: "Okay"
                            }).then(() => {
                                console.log("The user closed the alert.");
                            });
                        });
                }
            });
        });

    }

    setQueOnPage() {
        this.studentInfo = this.studentInfo[0];
        this.studentInfo.queRecord = [];
        this.queNumber = 1;
        this.nextBtnVisibility = true;
        this.totalQuestion = + this.questionsListObj.length;
        this.questionPosition = 1;

        this.questionNumber = this.questionsListObj[0].Id;
        this.setQueOptOnNext(this.questionNumber - 1);
        this.setQuestion(this.questionsListObj[this.queNumber - 1]);

        this.nextBtnClass = "btn btn-info btn-rounded-sm";
        this.backBtnClass = "btn btn-default btn-rounded-sm";

        // this.setBackandNextBtnVisibility();
        // this.startTimer();
        this.processing = false;
    }

    nextQuestion(queAskedForStd: string) {
        this.saveQueandAns().then(() => {
            this.queNumber = this.queNumber + 1;
            const questionObject = this.questionsListObj.filter((que: any) => {
                if (+que.Id === this.queNumber) {
                    return que;
                }
            });

            if (questionObject.length) {
                this.setQuestion(questionObject[0]);
            }

            this.setQueOptOnNext(this.questionNumber);
            this.questionNumber = questionObject[0].Id;
            this.questionPosition = this.questionPosition + 1;
            this.setBackandNextBtnVisibility();
        });
    }

    backQuestion() {

        this.queNumber = this.queNumber - 1;
        const questionObject = this.questionsListObj.filter((que: any) => {
            if (+que.Id === this.queNumber) {
                return que;
            }
        });

        if (questionObject.length) {
            this.setQuestion(questionObject[0]);
        }
        this.setQueOptOnBack(this.questionNumber);
        this.questionNumber = questionObject[0].Id;
        this.questionPosition = this.questionPosition - 1;
        this.setBackandNextBtnVisibility();
    }

    setQuestion(questionParam) {

        if (typeof questionParam === "object") {
            this.radioOptions = [];
            this.questionText = questionParam.QuestionText;

            if (questionParam.QuestionOptionData.length) {
                questionParam.QuestionOptionData.map((ansOpt) => {

                    if (ansOpt.OptionText) {
                        this.radioOptions.push(new RadioOption(ansOpt.OptionText));
                    }
                });
            }
        }
    }

    setBackandNextBtnVisibility() {

        if (this.questionPosition === 1 || this.questionNumber === 1) {
            this.backBtnVisibility = false;
            this.backBtnClass = "btn btn-default-disabled btn-rounded-sm";
        } else {
            this.backBtnClass = "btn btn-default btn-rounded-sm";
            this.backBtnVisibility = true;
        }

        if (this.questionsListObj.length === this.queNumber
            || this.questionNumber === this.questionsListObj.length) {
            this.nextBtnVisibility = false;
            this.nextBtnClass = "btn btn-info-disabled btn-rounded-sm";
            this.isSubmitBtnEnabled = true;
        } else {
            this.nextBtnClass = "btn btn-info btn-rounded-sm";
            this.nextBtnVisibility = true;
            this.isSubmitBtnEnabled = false;
        }
    }

    changeCheckedRadio(radioOption: RadioOption): void {
        this.stdAnswer = "";
        radioOption.selected = !radioOption.selected;

        if (!radioOption.selected) {
            return;
        }

        // uncheck all other options
        this.radioOptions.forEach((option) => {
            if (option.text !== radioOption.text) {
                option.selected = false;
            } else {
                option.selected = true;
            }
        });

        this.stdAnswer = radioOption.text;
    }

    checkedChange(modelRef) {
        console.log("checkedChange:", modelRef.checked);
    }

    startTimer() {
        this.countDown = timer(0, this.tick)
            .pipe(map(() => --this.testNameAndTime.testTime));
    }

    saveQueandAns() {
        return new Promise((resolve) => {
            const stdQueSheet = {
                queId: this.questionNumber,
                que: this.questionText,
                ans: this.stdAnswer
            };

            this._uiService.addPracExamQue(stdQueSheet, this.studentInfo).then(() => {
                return resolve();
            });
        });
    }

    setQueOptOnNext(queNumber: number) {
        this._uiService.readPracExamQue(queNumber + 1).then((res: any) => {
            if (res) {
                res.selected = false;
                res.text = res.ans;
                this.changeCheckedRadio(res);
            }
        });
    }

    setQueOptOnBack(queNumber: number) {
        this._uiService.readPracExamQue(queNumber - 1).then((res: any) => {
            if (res) {
                res.selected = false;
                res.text = res.ans;
                this.changeCheckedRadio(res);
            }
        });
    }

    submitQuestion() {
        this.processing = true;
        console.log("Practical exam has been submitted");
        // tslint:disable-next-line: max-line-length
        const stdFileName = `prc_${this.studentInfo.StudentUserIdId}_${this.studentInfo.StudentName}_${new Date()}_practical`;
        this._uiService.savePracExamQue(stdFileName).then((res: any) => {
            if (res.code === 200) {
                alert({
                    title: "Success",
                    message: "Your exam has been submitted successfully.",
                    okButtonText: "Okay"
                }).then(() => {
                    console.log("Closed popup after submit exam.");
                    const batchObj = JSON.parse(getString("batchlistobj"));
                    this._router.navigate([`/exam/practicalstudentlist/${batchObj.BatchId}`]);
                });

            } else {
                alert({
                    title: "Alert",
                    message: "There was some error while submitting the exam. Please try after sometime.",
                    okButtonText: "Okay"
                }).then(() => {
                    console.log("The user closed the alert.");
                });
            }

            this.processing = false;
        });
    }

}
