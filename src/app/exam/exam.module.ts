import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { BottomMenuModule } from "../shared/ui/bottom-menu/bottom-menu.module";

import { ActionBarModule } from "../shared/ui/action-bar/action-bar.module";
import { PreExamComponent } from "./pre-exam/pre-exam.component";
import { ExamComponent } from "./exam.component";
import { ExamRoutingModule } from "./exam-routing.module";
import { StudentListComponent } from "./theory-exam/student-list/student-list.component";
import { ExamGridComponent } from "./theory-exam/exam-grid/exam-grid.component";

import { PracStudentListComponent } from "./practical-exam/prac-student-list/prac-student-list.component";
import { PracExamSheetComponent } from "./practical-exam/prac-exam-sheet/prac-exam-sheet.component";

@NgModule({
    declarations: [
        PreExamComponent,
        ExamComponent,
        StudentListComponent,
        ExamGridComponent,
        PracStudentListComponent,
        PracExamSheetComponent
    ],
    imports: [
        NativeScriptCommonModule,
        NativeScriptUIListViewModule,
        ActionBarModule,
        ExamRoutingModule,
        BottomMenuModule
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class ExamModule { }
