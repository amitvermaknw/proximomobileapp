import { Component, OnInit, Input } from '@angular/core';
import { BatchEvidenceModules } from "../batch-evidence.modules";
import { DataService } from "../shared/services/data.service";

import * as uiHelper from "../../shared/ui/loader/loader";


@Component({
    moduleId: module.id,
    selector: 'ns-attendance-sheet',
    templateUrl: './attendance-sheet.component.html',
    styleUrls: ["../batch-evidence.component.android.scss"]
})
export class AttendanceSheetComponent implements OnInit {

    @Input() batchDetails: BatchEvidenceModules;
    attSheet1: boolean = true;
    attSheet2: boolean = true;

    cssforphoto1: string = "cam-layout";
    cssforphoto2: string = "cam-layout";

    constructor(
        private _dataService: DataService
    ) { }

    ngOnInit() {
        uiHelper.showLoader();
        this._dataService.fetchPhotoFile().then((res: any) => {
            if (res) {

                const imgDetail = JSON.parse(res);

                imgDetail.map((data) => {
                    if (data.type === "attendance"
                        && data.photoType === "one"
                        && this.batchDetails.BatchId === data.batchId) {

                        this.attSheet1 = false;
                        this.cssforphoto1 = "cam-layout-disabled";
                    } else if (data.type === "attendance"
                            && data.photoType === "two"
                            && this.batchDetails.BatchId === data.batchId) {
                        this.attSheet2 = false;
                        this.cssforphoto2 = "cam-layout-disabled";
                    }

                });

            } else {
                this.cssforphoto1 = "cam-layout";
                this.cssforphoto2 = "cam-layout";
            }

            uiHelper.hideLoader();
        })
        .catch((err) => {
            console.log("Error while fetching photo summary file while loading" + err);
        });
    }

    takePhoto(type) {

        uiHelper.showLoader();
        const batchDetail = {
            type: "attendance",
            batchName: this.batchDetails.BatchName,
            batchId: this.batchDetails.BatchId,
            photoType: type,
            videoType: ""
        };

        this._dataService.savePhotos(batchDetail).then((res: any) => {
            if (res) {
                const imgDetail = JSON.parse(res);

                imgDetail.map((data) => {
                    if (data.type === "attendance"
                        && data.photoType === "one"
                        && this.batchDetails.BatchId === data.batchId) {
                        this.attSheet1 = false;
                        this.cssforphoto1 = "cam-layout-disabled";
                    } else if (data.type === "attendance"
                        && data.photoType === "two"
                        && this.batchDetails.BatchId === data.batchId) {
                        this.attSheet2 = false;
                        this.cssforphoto2 = "cam-layout-disabled";
                    }

                });
            }

            uiHelper.hideLoader();

        }).catch((err) => {
            console.log(err);
        });
    }

}
