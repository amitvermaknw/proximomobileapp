import { Injectable, ViewContainerRef } from "@angular/core";
import { LocalStorage } from "./local-storage.service";


@Injectable({
    providedIn: "root"
})
export class PhotosService {

    private photos: string[] = [
    ];
    private photoExamples: string[] = [
    ];

    constructor(
        private localStorage: LocalStorage
    ) { }


    getPhotos() {
        return this.photos;
    }

    addPhoto() {

        const photoToAdd: string = this.photoExamples[Math.floor(Math.random() * (this.photoExamples.length - 1)) + 1];
        if (photoToAdd != '') {
            this.photos.unshift(photoToAdd);
            this.localStorage.saveValue(JSON.stringify(this.photos), 'photos');
        }
    }

    getFromLocalStorage() {
        if (!this.localStorage.getValue('photos')) {
            // console.log('FIRST TIME, SAVING VALUES');
            this.localStorage.saveValue(JSON.stringify(this.photos), 'photos');
        } else {
            // console.log('NOT FIRST TIME, GETTING VALUES');
            this.photos = JSON.parse(this.localStorage.getValue('photos'));
        }

    }
}
