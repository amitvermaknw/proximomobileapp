import { Injectable, resolveForwardRef } from '@angular/core';
import { Subject } from 'rxjs';
import { BatchEvidenceModules, BatchPhotoDetails } from "../../batch-evidence.modules";

import { ImageSource } from "tns-core-modules/image-source/image-source";
import * as fs from "tns-core-modules/file-system";
import { CameraService } from "./camera.service";
import { ModalDialogOptions, ModalDialogService } from "nativescript-angular/modal-dialog";
import { ImageAsset } from "tns-core-modules/image-asset";


@Injectable({
    providedIn: 'root'
})

export class DataService {

    batchData = new Subject<BatchEvidenceModules>();
    folder: fs.Folder;
    file: fs.File;

    private takenPhoto: ImageAsset;

    constructor(
        private camera: CameraService,
        // private vref: ViewContainerRef,
        private modal: ModalDialogService
    ) { }

    savePhotos(batchDetails: BatchPhotoDetails) {

        return new Promise((resolve) => {
            const source = new ImageSource();
            this.camera.takePhoto()
                .then((imageAsset) => {

                    /*const options: ModalDialogOptions = {
                        context: imageAsset,
                        viewContainerRef: this.vref,
                        fullscreen: true
                    };*/

                    source.fromAsset(imageAsset).then((imageSource: ImageSource) => {

                        if (fs.knownFolders.documents().contains("proximo")) {
                            // tslint:disable-next-line: max-line-length
                            const assessorImgFolderPath = fs.path.join(fs.knownFolders.documents().getFolder("proximo").path, "BatchImages");
                            const assessorImgFolder = fs.Folder.fromPath(assessorImgFolderPath);

                            const fileName = `${batchDetails.batchName}${Math.round(new Date().getTime() / 1000)}.jpg`;
                            const filePath = fs.path.join(assessorImgFolder.path, fileName);
                            const saved: boolean = imageSource.saveToFile(filePath, "jpg");
                            const imgJSONFilePath = fs.path.join(assessorImgFolderPath, "savedImgFile.json");
                            const imageJSONExist: boolean = fs.File.exists(imgJSONFilePath);

                            const imageJSONContent = {
                                type: batchDetails.type,
                                path: filePath,
                                date: new Date("MM/DD/YYYY"),
                                name: fileName,
                                batchName: batchDetails.batchName,
                                batchId: batchDetails.batchId,
                                photoType: batchDetails.photoType
                            };

                            let imageJSONFile;

                            if (!imageJSONExist) {
                                imageJSONFile = assessorImgFolder.getFile("savedImgFile.json");

                                const content = [];
                                content.push(imageJSONContent);

                                const imageDetail = JSON.stringify(content);

                                // console.log("Content = " + imageDetail);
                                imageJSONFile.writeText(imageDetail).then((result) => {
                                    console.log("Image detail is added first time in json file");

                                    imageJSONFile.readText().then((jsonContent: any) => {
                                        return resolve(jsonContent);
                                    });

                                }).catch((err) => {
                                    console.log(err);

                                    return resolve(false);
                                });
                            } else {

                                imageJSONFile = assessorImgFolder.getFile("savedImgFile.json");

                                imageJSONFile.readText().then((existingContent: any) => {

                                    if (existingContent) {
                                        let addedNewContentArr = [];
                                        addedNewContentArr = JSON.parse(existingContent);
                                        addedNewContentArr.push(imageJSONContent);

                                        const addedNewContent = JSON.stringify(addedNewContentArr);

                                        imageJSONFile.writeText(addedNewContent).then((result) => {
                                            // console.log("Image detail is updated in json file");

                                            imageJSONFile.readText().then((jsonContent: any) => {
                                                return resolve(jsonContent);
                                            });

                                        }).catch((err) => {
                                            console.log(err);

                                            return resolve(false);
                                        });
                                    }

                                }).catch((err) => {
                                    console.log(err.stack);

                                    return resolve(false);
                                });

                            }

                            if (saved) {
                                console.log("Image saved");
                            }
                        }
                    });

                    /*setTimeout(() => {
                        this.modal.showModal(FilterComponent, options).then((response) => {
                            if (response == 'success') {
                                // this.onNavtap('profile', '4');
                            }
                            else {
                                // this.onNavtap('home', '0');
                            }
                        }, error => {
                            console.log(error);
                        });
                    }, 1000);*/

                }).catch((err) => {
                    console.log(err.message);

                    return resolve(false);
                });
        });
    }

    fetchPhotoFile() {

        return new Promise((resolve) => {
            if (fs.knownFolders.documents().contains("proximo")) {
                // tslint:disable-next-line: max-line-length
                const assessorImgFolderPath = fs.path.join(fs.knownFolders.documents().getFolder("proximo").path, "BatchImages");
                const assessorImgFolder = fs.Folder.fromPath(assessorImgFolderPath);
                const imgJSONFilePath = fs.path.join(assessorImgFolderPath, "savedImgFile.json");
                const imageJSONExist: boolean = fs.File.exists(imgJSONFilePath);

                if (imageJSONExist) {
                    const imageJSONFile = assessorImgFolder.getFile("savedImgFile.json");

                    imageJSONFile.readText().then((existingContent: any) => {
                        if (existingContent) {
                            return resolve(existingContent);
                        } else {
                            return resolve(false);
                        }

                    }).catch((err) => {
                        console.log(err.stack);

                        return resolve(false);
                    });
                } else {
                    return resolve(false);
                }

            } else {
                return resolve(false);
            }
        });

    }

    deletePhotoSummaryFile() {

        return new Promise((resolve) => {
            if (fs.knownFolders.documents().contains("proximo")) {
                // tslint:disable-next-line: max-line-length
                const assessorImgFolderPath = fs.path.join(fs.knownFolders.documents().getFolder("proximo").path, "BatchImages");
                const assessorImgFolder = fs.Folder.fromPath(assessorImgFolderPath);
                const imgJSONFilePath = fs.path.join(assessorImgFolderPath, "savedImgFile.json");
                const imageJSONExist: boolean = fs.File.exists(imgJSONFilePath);

                if (imageJSONExist) {
                    const imageJSONFile = assessorImgFolder.getFile("savedImgFile.json");

                    imageJSONFile.remove().then((res) => {
                        console.log("Summary File removed successfully    ");

                        return resolve(true);

                    }).catch((err) => {
                        console.log(err.stack);

                        return resolve(false);
                    });
                } else {
                    return resolve(false);
                }

            } else {
                return resolve(false);
            }
        });

    }

    saveVideos(batchDetails: BatchPhotoDetails) {

        return new Promise((resolve) => {
            this.camera.takeVideo()
                .then((videoPath: any) => {

                    if (videoPath) {

                        if (fs.knownFolders.documents().contains("proximo")) {
                            // tslint:disable-next-line: max-line-length
                            const assessorImgFolderPath = fs.path.join(fs.knownFolders.documents().getFolder("proximo").path, "BatchImages");
                            const assessorImgFolder = fs.Folder.fromPath(assessorImgFolderPath);

                            const filePath = fs.path.join(assessorImgFolder.path, videoPath);
                            // const saved: boolean = imageSource.saveToFile(filePath, "jpg");
                            const imgJSONFilePath = fs.path.join(assessorImgFolderPath, "savedImgFile.json");
                            const imageJSONExist: boolean = fs.File.exists(imgJSONFilePath);

                            const imageJSONContent = {
                                type: batchDetails.type,
                                path: videoPath,
                                date: new Date("MM/DD/YYYY"),
                                name: videoPath,
                                batchName: batchDetails.batchName,
                                batchId: batchDetails.batchId,
                                photoType: batchDetails.photoType,
                                videoType: batchDetails.videoType
                            };

                            let imageJSONFile;

                            if (!imageJSONExist) {
                                imageJSONFile = assessorImgFolder.getFile("savedImgFile.json");

                                const content = [];
                                content.push(imageJSONContent);

                                const imageDetail = JSON.stringify(content);

                                // console.log("Content = " + imageDetail);
                                imageJSONFile.writeText(imageDetail).then((result) => {
                                    console.log("Image detail is added first time in json file");

                                    imageJSONFile.readText().then((jsonContent: any) => {
                                        return resolve(jsonContent);
                                    });

                                }).catch((err) => {
                                    console.log(err);

                                    return resolve(false);
                                });
                            } else {

                                imageJSONFile = assessorImgFolder.getFile("savedImgFile.json");

                                imageJSONFile.readText().then((existingContent: any) => {

                                    if (existingContent) {
                                        let addedNewContentArr = [];
                                        addedNewContentArr = JSON.parse(existingContent);
                                        addedNewContentArr.push(imageJSONContent);

                                        const addedNewContent = JSON.stringify(addedNewContentArr);

                                        imageJSONFile.writeText(addedNewContent).then((result) => {
                                            // console.log("Image detail is updated in json file");

                                            imageJSONFile.readText().then((jsonContent: any) => {
                                                return resolve(jsonContent);
                                            });

                                        }).catch((err) => {
                                            console.log(err);

                                            return resolve(false);
                                        });
                                    }

                                }).catch((err) => {
                                    console.log(err.stack);

                                    return resolve(false);
                                });

                            }
                        }
                    } else {
                        console.log("Video Saved");
                    }

                }).catch((err) => {
                    console.log(err.message);

                    return resolve(false);
                });
        });
    }

    fetchVideoFile() {

        return new Promise((resolve) => {
            if (fs.knownFolders.documents().contains("proximo")) {
                // tslint:disable-next-line: max-line-length
                const assessorImgFolderPath = fs.path.join(fs.knownFolders.documents().getFolder("proximo").path, "BatchImages");
                const assessorImgFolder = fs.Folder.fromPath(assessorImgFolderPath);
                const imgJSONFilePath = fs.path.join(assessorImgFolderPath, "savedImgFile.json");
                const imageJSONExist: boolean = fs.File.exists(imgJSONFilePath);

                if (imageJSONExist) {
                    const imageJSONFile = assessorImgFolder.getFile("savedImgFile.json");

                    imageJSONFile.readText().then((existingContent: any) => {
                        if (existingContent) {
                            return resolve(existingContent);
                        } else {
                            return resolve(false);
                        }

                    }).catch((err) => {
                        console.log(err.stack);

                        return resolve(false);
                    });
                } else {
                    return resolve(false);
                }

            } else {
                return resolve(false);
            }
        });

    }

}
