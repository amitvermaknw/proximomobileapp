import { takePicture, requestPermissions, isAvailable } from "nativescript-camera";
import { Injectable } from "@angular/core";

import { VideoRecorder } from "nativescript-videorecorder";
import * as Permissions from "nativescript-permissions";
import { isAndroid } from "tns-core-modules/platform";
import * as dialogs from "tns-core-modules/ui/dialogs";

declare const android: any;

@Injectable({
    providedIn: "root"
})
export class CameraService {

    saveToGallery: boolean = true;
    keepAspectRatio: boolean = true;
    width: number = 200;
    height: number = 200;

    private _recorder = new VideoRecorder({
        format: "mp4",
        saveToGallery: true,
        hd: true,
        explanation: "We need to be able to record video"
    });

    constructor() {
        requestPermissions();
    }

    takePhoto() {

        const options = {
            width: this.width,
            height: this.height,
            keepAspectRatio: this.keepAspectRatio,
            saveToGallery: this.saveToGallery,
            enableVideo: true
        };

        return takePicture(options);

    }

    onCheckForCamera() {
        const isCameraAvailable = isAvailable();
        console.log("Is camera hardware available: " + isCameraAvailable);
    }

    takeVideo() {
        return new Promise((resolve) => {
            if (isAndroid) {
                Permissions.requestPermission([android.Manifest.permission.RECORD_AUDIO, android.Manifest.permission.CAMERA],
                    "Demo needs Audio and Camera permissions to record a video")
                    .then(() => {
                        this._takeVideo().then((res) => {
                            return resolve(res);
                        });
                    }, (error) => this._error("Demo needs Audio and Camera permissions to record a video"));
            } else {
                this._takeVideo().then((res) => {
                    return resolve(res);
                });
            }
        });
    }

    private _takeVideo() {
        return new Promise((resolve) => {
            this._recorder.record()
            .then((data) => {
                console.log("Original file size ");

                return resolve(data.file);
            })
            .catch((error) => { if (error.event !== "cancelled") { this._error("Couldn't record your video"); } });
        });
    }

    private _error(error: string | Error): Promise<void> {
        return dialogs.alert({
            title: "Uh oh...",
            message: (error instanceof Error) ? error.message : error,
            okButtonText: "OK, got it"
        });
    }

}
