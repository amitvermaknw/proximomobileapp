import { Component, OnInit, Input, ViewContainerRef } from "@angular/core";
import { BatchEvidenceModules } from "../batch-evidence.modules";
import { DataService } from "../shared/services/data.service";
import { ModalDialogService } from "nativescript-angular/modal-dialog";

import * as uiHelper from "../../shared/ui/loader/loader";


@Component({
    moduleId: module.id,
    selector: 'ns-theory-video',
    templateUrl: './theory-video.component.html',
    styleUrls: ["../batch-evidence.component.android.scss"]
})
export class TheoryVideoComponent implements OnInit {

    @Input() batchDetails: BatchEvidenceModules;
    theoryVideo1: boolean = true;
    theoryVideo2: boolean = true;

    cssforTheoryVideo1: string = "cam-layout";
    cssforTheoryVideo2: string = "cam-layout";

    constructor(
        private _dataService: DataService,
        private modalDialog: ModalDialogService,
        private vcRef: ViewContainerRef
    ) { }

    ngOnInit() {

        uiHelper.showLoader();
        this._dataService.fetchVideoFile().then((res: any) => {
            if (res) {

                const imgDetail = JSON.parse(res);
                console.log("videoFilePath = " + res);
                imgDetail.map((data) => {
                    if (data.type === "theoryvideo"
                        && data.videoType === "one"
                        && this.batchDetails.BatchId === data.batchId) {

                        this.theoryVideo1 = false;
                        this.cssforTheoryVideo1 = "cam-layout-disabled";
                    } else if (data.type === "theoryvideo"
                            && data.videoType === "two"
                            && this.batchDetails.BatchId === data.batchId) {
                        this.theoryVideo2 = false;
                        this.cssforTheoryVideo2 = "cam-layout-disabled";
                    }

                });

            } else {
                this.cssforTheoryVideo1 = "cam-layout";
                this.cssforTheoryVideo2 = "cam-layout";
            }

            uiHelper.hideLoader();
        })
        .catch((err) => {
            console.log("Error while fetching photo summary file while loading" + err);
        });
    }

    saveVideo(type) {
        uiHelper.showLoader();
        const batchDetail = {
            type: "theoryvideo",
            batchName: this.batchDetails.BatchName,
            batchId: this.batchDetails.BatchId,
            videoType: type,
            photoType: ""
        };

        this._dataService.saveVideos(batchDetail).then((res: any) => {
            if (res) {
                const imgDetail = JSON.parse(res);

                imgDetail.map((data) => {
                    if (data.type === "theoryvideo"
                        && data.videoType === "one"
                        && this.batchDetails.BatchId === data.batchId) {
                        this.theoryVideo1 = false;
                        this.cssforTheoryVideo1 = "cam-layout-disabled";
                    } else if (data.type === "theoryvideo"
                        && data.videoType === "two"
                        && this.batchDetails.BatchId === data.batchId) {
                        this.theoryVideo2 = false;
                        this.cssforTheoryVideo2 = "cam-layout-disabled";
                    }

                });
            }

            uiHelper.hideLoader();

        }).catch((err) => {
            console.log(err);
        });
    }

}
