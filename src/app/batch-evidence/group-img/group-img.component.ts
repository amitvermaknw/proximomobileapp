import { Component, OnInit, Input } from '@angular/core';
import { BatchEvidenceModules } from "../batch-evidence.modules";
import { DataService } from "../shared/services/data.service";

import * as uiHelper from "../../shared/ui/loader/loader";

@Component({
    moduleId: module.id,
    selector: 'ns-group-img',
    templateUrl: './group-img.component.html',
    styleUrls: ["../batch-evidence.component.android.scss"]
})
export class GroupImgComponent implements OnInit {

    @Input() batchDetails: BatchEvidenceModules;
    groupImg1: boolean = true;
    groupImg2: boolean = true;

    cssforGroupImg1: string = "cam-layout";
    cssforGroupImg2: string = "cam-layout";

    constructor(
        private _dataService: DataService
    ) { }

    ngOnInit() {

        uiHelper.showLoader();
        this._dataService.fetchPhotoFile().then((res: any) => {
            if (res) {

                const imgDetail = JSON.parse(res);

                imgDetail.map((data) => {
                    if (data.type === "groupimg"
                        && data.photoType === "one"
                        && this.batchDetails.BatchId === data.batchId) {

                        this.groupImg1 = false;
                        this.cssforGroupImg1 = "cam-layout-disabled";
                    } else if (data.type === "groupimg"
                            && data.photoType === "two"
                            && this.batchDetails.BatchId === data.batchId) {
                        this.groupImg2 = false;
                        this.cssforGroupImg2 = "cam-layout-disabled";
                    }

                });

            } else {
                this.cssforGroupImg1 = "cam-layout";
                this.cssforGroupImg2 = "cam-layout";
            }

            uiHelper.hideLoader();
        })
        .catch((err) => {
            console.log("Error while fetching photo summary file while loading" + err);
        });
    }

    takePhoto(type) {

        uiHelper.showLoader();
        const batchDetail = {
            type: "groupimg",
            batchName: this.batchDetails.BatchName,
            batchId: this.batchDetails.BatchId,
            photoType: type,
            videoType: ""
        };

        this._dataService.savePhotos(batchDetail).then((res: any) => {
            if (res) {
                const imgDetail = JSON.parse(res);

                imgDetail.map((data) => {
                    if (data.type === "groupimg"
                        && data.photoType === "one"
                        && this.batchDetails.BatchId === data.batchId) {
                        this.groupImg1 = false;
                        this.cssforGroupImg1 = "cam-layout-disabled";
                    } else if (data.type === "groupimg"
                        && data.photoType === "two"
                        && this.batchDetails.BatchId === data.batchId) {
                        this.groupImg2 = false;
                        this.cssforGroupImg2 = "cam-layout-disabled";
                    }

                });
            }

            uiHelper.hideLoader();

        }).catch((err) => {
            console.log(err);
        });
    }

}
