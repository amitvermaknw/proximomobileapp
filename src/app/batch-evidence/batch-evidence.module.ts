import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { BottomMenuModule } from "../shared/ui/bottom-menu/bottom-menu.module";

import { BatchEvidenceRoutingModule } from "./batch-evidence-routing.module";
import { BatchEvidenceComponent } from "./batch-evidence.component";
import { AssessorImgComponent } from "./assessor-img/assessor-img.component";
import { AttendanceSheetComponent } from "./attendance-sheet/attendance-sheet.component";
import { GroupImgComponent } from "./group-img/group-img.component";
import { TheoryImgComponent } from "./theory-img/theory-img.component";
import { TheoryVideoComponent } from "./theory-video/theory-video.component";
import { ActionBarModule } from "../shared/ui/action-bar/action-bar.module";
import { FilterComponent } from "./shared/filter/filter.component";

@NgModule({
    declarations: [
        BatchEvidenceComponent,
        AssessorImgComponent,
        TheoryVideoComponent,
        TheoryImgComponent,
        GroupImgComponent,
        AttendanceSheetComponent,
        FilterComponent
    ],
    imports: [
        NativeScriptCommonModule,
        BatchEvidenceRoutingModule,
        ActionBarModule,
        BottomMenuModule
    ],
    entryComponents: [
        FilterComponent
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class BatchEvidenceModule { }
