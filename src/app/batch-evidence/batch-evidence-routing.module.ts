import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { Routes } from "@angular/router";
import { BatchEvidenceComponent } from './batch-evidence.component'
import { NativeScriptRouterModule } from 'nativescript-angular';

const routes: Routes = [
    { path: ":id", component: BatchEvidenceComponent }
];

@NgModule({
  declarations: [],
  imports: [
    NativeScriptCommonModule,
    NativeScriptRouterModule.forChild(routes)
  ],
  exports: [NativeScriptRouterModule],
  schemas: [NO_ERRORS_SCHEMA]
})
export class BatchEvidenceRoutingModule { }
