import { Component, OnInit, Input } from "@angular/core";
import { BatchEvidenceModules } from "../batch-evidence.modules";
import { DataService } from "../shared/services/data.service";

import * as uiHelper from "../../shared/ui/loader/loader";

@Component({
    moduleId: module.id,
    selector: "ns-assessor-img",
    templateUrl: "./assessor-img.component.html",
    styleUrls: ["../batch-evidence.component.android.scss"]
})
export class AssessorImgComponent implements OnInit {

    @Input() batchDetails: BatchEvidenceModules;
    assessorIN: boolean = true;
    assessorOUT: boolean = true;

    stackLauoutClassIn: string = "cam-layout";
    stackLauoutClassOut: string = "cam-layout";

    constructor(
        private _dataService: DataService
    ) { }

    ngOnInit() {

        uiHelper.showLoader();

        this._dataService.fetchPhotoFile().then((res: any) => {
            if (res) {

                const imgDetail = JSON.parse(res);

                imgDetail.map((data) => {
                    if (data.type === "assessor"
                        && data.photoType === "in"
                        && this.batchDetails.BatchId === data.batchId) {

                        this.assessorIN = false;
                        this.stackLauoutClassIn = "cam-layout-disabled";
                    } else if (data.type === "assessor"
                            && data.photoType === "out"
                            && this.batchDetails.BatchId === data.batchId) {
                        this.assessorOUT = false;
                        this.stackLauoutClassOut = "cam-layout-disabled";
                    }

                });

            } else {
                this.stackLauoutClassIn = "cam-layout";
                this.stackLauoutClassOut = "cam-layout";
            }

            uiHelper.hideLoader();
        })
        .catch((err) => {
            console.log("Error while fetching photo summary file while loading" + err);
        });
    }

    takePhoto(type) {

        uiHelper.showLoader();
        const batchDetail = {
            type: "assessor",
            batchName: this.batchDetails.BatchName,
            batchId: this.batchDetails.BatchId,
            photoType: type,
            videoType: ""
        };

        this._dataService.savePhotos(batchDetail).then((res: any) => {
            if (res) {
                const imgDetail = JSON.parse(res);

                imgDetail.map((data) => {
                    if (data.type === "assessor"
                        && data.photoType === "in"
                        && this.batchDetails.BatchId === data.batchId) {
                        this.assessorIN = false;
                        this.stackLauoutClassIn = "cam-layout-disabled";
                    } else if (data.type === "assessor"
                        && data.photoType === "out"
                        && this.batchDetails.BatchId === data.batchId) {
                        this.assessorOUT = false;
                        this.stackLauoutClassOut = "cam-layout-disabled";
                    }

                });
            }

            uiHelper.hideLoader();

        }).catch((err) => {
            console.log(err);
        });
    }

    deletePhotoFile() {
        this._dataService.deletePhotoSummaryFile().then((res) => {
            if (res) {
                console.log("File deleted successfully.....");
            }
        });
    }

}
