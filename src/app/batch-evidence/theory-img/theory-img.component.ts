import { Component, OnInit, Input } from "@angular/core";
import { BatchEvidenceModules } from "../batch-evidence.modules";
import { DataService } from "../shared/services/data.service";
import * as uiHelper from "../../shared/ui/loader/loader";

@Component({
    moduleId: module.id,
    selector: 'ns-theory-img',
    templateUrl: './theory-img.component.html',
    styleUrls: ["../batch-evidence.component.android.scss"]
})
export class TheoryImgComponent implements OnInit {

    @Input() batchDetails: BatchEvidenceModules;
    theoryImg1: boolean = true;
    theoryImg2: boolean = true;

    cssforTheory1: string = "cam-layout";
    cssforTheory2: string = "cam-layout";

    constructor(
        private _dataService: DataService
    ) { }

    ngOnInit() {

        uiHelper.showLoader();
        this._dataService.fetchPhotoFile().then((res: any) => {
            if (res) {

                const imgDetail = JSON.parse(res);

                imgDetail.map((data) => {
                    if (data.type === "theoryimg"
                        && data.photoType === "one"
                        && this.batchDetails.BatchId === data.batchId) {

                        this.theoryImg1 = false;
                        this.cssforTheory1 = "cam-layout-disabled";
                    } else if (data.type === "theoryimg"
                            && data.photoType === "two"
                            && this.batchDetails.BatchId === data.batchId) {
                        this.theoryImg2 = false;
                        this.cssforTheory2 = "cam-layout-disabled";
                    }

                });

            } else {
                this.cssforTheory1 = "cam-layout";
                this.cssforTheory2 = "cam-layout";
            }

            uiHelper.hideLoader();
        })
        .catch((err) => {
            console.log("Error while fetching photo summary file while loading" + err);
        });
    }

    takePhoto(type) {

        uiHelper.showLoader();
        const batchDetail = {
            type: "theoryimg",
            batchName: this.batchDetails.BatchName,
            batchId: this.batchDetails.BatchId,
            photoType: type,
            videoType: ""
        };

        this._dataService.savePhotos(batchDetail).then((res: any) => {
            if (res) {
                const imgDetail = JSON.parse(res);

                imgDetail.map((data) => {
                    if (data.type === "theoryimg"
                        && data.photoType === "one"
                        && this.batchDetails.BatchId === data.batchId) {
                        this.theoryImg1 = false;
                        this.cssforTheory1 = "cam-layout-disabled";
                    } else if (data.type === "theoryimg"
                        && data.photoType === "two"
                        && this.batchDetails.BatchId === data.batchId) {
                        this.theoryImg2 = false;
                        this.cssforTheory2 = "cam-layout-disabled";
                    }

                });
            }

            uiHelper.hideLoader();

        }).catch((err) => {
            console.log(err);
        });
    }

}
