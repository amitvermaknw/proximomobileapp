
export class BatchEvidenceModules {

    BatchId: number;
    BatchName: string;
    TotalStudent: number;
    TrainingCenter: string;
    TrainingPartner?: string;
    CreatedOn?: string;
    TestName: string;
    Status?: number;
    Message?: [];
    constructor(batchList) {
        this.BatchId = batchList.batchId;
        this.BatchName = batchList.batchName;
        this.TotalStudent = batchList.totalStudent;
        this.TrainingCenter = batchList.trainingCenter;
        this.TrainingPartner = batchList.trainingPartner;
        this.CreatedOn = batchList.createdOn;
        this.TestName = batchList.testName;
        this.Status = batchList.Status;
        this.Message = batchList.Message;
    }
}

// tslint:disable-next-line: max-classes-per-file
export class BatchPhotoDetails {

    type: string;
    batchId: number;
    batchName: string;
    photoType: string;
    videoType: string;
    constructor(batchList) {
        this.type = batchList.type;
        this.batchId = batchList.batchId;
        this.batchName = batchList.batchName;
        this.photoType = batchList.photoType;
        this.videoType = batchList.videoType;
    }
}
