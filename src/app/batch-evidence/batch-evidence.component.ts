import { Component, OnInit } from "@angular/core";
import { PageRoute, RouterExtensions } from "nativescript-angular";
import { knownFolders, File, Folder } from "tns-core-modules/file-system";
import { BatchEvidenceModules } from "./batch-evidence.modules";

import { ApiService } from "../shared/services/api.service";
import { UIServices } from "../shared/ui/ui.services";
import * as uiHelper from "../shared/ui/loader/loader";

@Component({
    moduleId: module.id,
    selector: 'ns-batch-evidence',
    templateUrl: './batch-evidence.component.html',
    styleUrls: ['./batch-evidence.component.android.scss']
})

export class BatchEvidenceComponent implements OnInit {

    documents = knownFolders.documents();
    folder: Folder;
    file: File;
    batchListObj: BatchEvidenceModules;
    categoriesIcons: {};
    processing: boolean;

    constructor(
        private _pageRoute: PageRoute,
        private _apiService: ApiService,
        private _uiServices: UIServices,
        private _router: RouterExtensions
    ) {
        this.categoriesIcons = this._uiServices.getCategoriesIcons();

    }

    ngOnInit() {
        this.processing = true;
        console.log("indide Evidence   ");
        this._pageRoute.activatedRoute.subscribe((activatedRoute) => {
            activatedRoute.paramMap.subscribe((paramMap) => {
                const batchId = +paramMap.get("id");
                if (batchId) {

                    this.folder = this.documents.getFolder("proximo");
                    this.file = this.folder.getFile("batchlist.json");

                    this.file.readText().then((result) => {

                        const batchListData = JSON.parse(result);
                        if (batchListData.length) {

                            this.batchListObj = batchListData[0].Message.filter((res) => {
                                if (res.BatchId === batchId.toString()) {
                                    return res;
                                }
                            });

                            this.batchListObj = this.batchListObj[0];
                            this.processing = false;
                            uiHelper.hideLoader();
                        }

                    }).catch((err) => {
                        console.log(err);
                    });
                }
            });

        });
    }

    getBadgeIcon(id: string) {
        return this.categoriesIcons[id];
    }

    nextScreen() {
        // this._apiService.batchDetail.next(this.batchListObj);
        this._router.navigate([`/exam/preexam`], { state: this.batchListObj});
    }

}
