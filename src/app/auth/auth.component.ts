import { Component, OnInit, ElementRef, ViewChild, OnDestroy } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { ApiService } from "../shared/services/api.service";
import { alert, prompt } from "tns-core-modules/ui/dialogs";
import { Page } from "tns-core-modules/ui/page";
import { TextField } from "tns-core-modules/ui/text-field";

import { User } from "./auth.modules";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { UIServices } from "../shared/ui/ui.services";
import * as uiHelper from "../shared/ui/loader/loader";

@Component({
    selector: "ns-auth",
    moduleId: module.id,
    templateUrl: "./auth.component.html",
    styleUrls: ["./auth.component.android.scss"]
})

export class AuthComponent implements OnInit, OnDestroy {

    isLoggingIn = true;
    user: User;
    processing = false;
    form: FormGroup;
    emailControlIsValid = true;
    passwordControlIsValid = true;

    @ViewChild("passwordEl", { static: false }) passwordEl: ElementRef<TextField>;
    @ViewChild("userNameEl", { static: false }) userNameEl: ElementRef<TextField>;

    @ViewChild("password", { static: false }) password: ElementRef;
    @ViewChild("confirmPassword", { static: false }) confirmPassword: ElementRef;

    constructor(
        private page: Page,
        private userService: ApiService,
        private router: RouterExtensions,
        private _apiService: ApiService,
        private _uiService: UIServices) {
        this.page.actionBarHidden = true;
    }

    ngOnInit() {
        this.form = new FormGroup({
            username: new FormControl(null, {
                updateOn: "blur",
                validators: [Validators.required]
            }),
            password: new FormControl(null, {
                updateOn: "blur",
                validators: [Validators.required]
            })
        });

        this.form.get("username").statusChanges.subscribe((status) => {
            this.emailControlIsValid = status === 'VALID';
        });

        this.form.get("password").statusChanges.subscribe(status => {
            this.passwordControlIsValid = status === 'VALID';
        });
    }

    toggleForm() {
        this.isLoggingIn = !this.isLoggingIn;
    }

    submit() {

        this.processing = true;
        uiHelper.showLoader();
        this.userNameEl.nativeElement.focus();
        this.passwordEl.nativeElement.focus();
        this.passwordEl.nativeElement.dismissSoftInput();

        if (!this.form.valid) {
            this.processing = false;
            this.emailControlIsValid = false;
            this.passwordControlIsValid = false;

            return;
        }

        const userName = this.form.get("username").value;
        const password = this.form.get("password").value;
        // this.form.reset();
        this.emailControlIsValid = true;
        this.passwordControlIsValid = true;

        // this.router.navigate([`/setting`], { clearHistory: true });

        setTimeout(() => {
            this._apiService.checkAuth(userName, password).subscribe(
                (res) => {
                    if (res) {
                        const response = res[0];
                        if (response.Status === "200") {
                            if (response.Message.length) {
                                const assessorId = response.Message[0].UserId;

                                // this.router.navigate([`/setting`], { clearHistory: true });
                                const userCredential = {username: userName, password: password}
                                this._uiService.writeFiles("proximo", "credential",
                                    JSON.stringify(userCredential)).then((response: any) => {
                                        if (response.code === 200) {
                                            console.log("Credential has been stored in file");
                                        }
                                });

                                this._uiService.writeFiles("proximo", "logondetails",
                                    JSON.stringify(res)).then((response: any) => {
                                        if (response.code === 200) {
                                            console.log("logindetail has been stored in file");
                                        }
                                });

                                this.router.navigate([`/batchlist/${assessorId}`], { clearHistory: true });
                                this.processing = false;
                                uiHelper.hideLoader();
                            }
                        } else {
                            uiHelper.hideLoader();
                        }

                    } else {
                        alert({
                            title: "Alert",
                            message: "Logon id or password is incorrect.",
                            okButtonText: "Okay"
                        }).then(() => {
                            // console.log("The user closed the alert.");
                        });
                        uiHelper.hideLoader();
                    }

                },
                (err) => {
                    console.log(err);
                    uiHelper.hideLoader();
                }
            );
        });
    }

    ngOnDestroy() {
        uiHelper.hideLoader();
    }

    /*login() {
        this.userService.login(this.user)
            .then(() => {
                this.processing = false;
                this.routerExtensions.navigate(["/home"], { clearHistory: true });
            })
            .catch(() => {
                this.processing = false;
                this.alert("Unfortunately we could not find your account.");
            });
    }

    register() {
        if (this.user.password != this.user.confirmPassword) {
            this.alert("Your passwords do not match.");
            return;
        }
        this.userService.register(this.user)
            .then(() => {
                this.processing = false;
                this.alert("Your account was successfully created.");
                this.isLoggingIn = true;
            })
            .catch(() => {
                this.processing = false;
                this.alert("Unfortunately we were unable to create your account.");
            });
    }

    forgotPassword() {
        prompt({
            title: "Forgot Password",
            message: "Enter the email address you used to register for APP NAME to reset your password.",
            inputType: "email",
            defaultText: "",
            okButtonText: "Ok",
            cancelButtonText: "Cancel"
        }).then((data) => {
            if (data.result) {
                this.userService.resetPassword(data.text.trim())
                    .then(() => {
                        this.alert("Your password was successfully reset. Please check your email for instructions on choosing a new password.");
                    }).catch(() => {
                        this.alert("Unfortunately, an error occurred resetting your password.");
                    });
            }
        });
    }*/

    focusPassword() {
        // this.password.nativeElement.focus();
    }
    focusConfirmPassword() {
        if (!this.isLoggingIn) {
            // this.confirmPassword.nativeElement.focus();
        }
    }

    alert(message: string) {
        return alert({
            title: "APP NAME",
            okButtonText: "OK",
            message: message
        });
    }
}
