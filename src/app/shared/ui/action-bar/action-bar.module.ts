import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { ActionBarComponent } from "./action-bar.component";

@NgModule({
    imports: [
        NativeScriptCommonModule
    ],
    declarations: [
        ActionBarComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    exports: [
        ActionBarComponent
    ]
})
export class ActionBarModule { }
