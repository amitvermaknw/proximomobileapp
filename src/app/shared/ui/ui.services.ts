import { Injectable, ViewContainerRef } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { knownFolders, File, Folder } from "tns-core-modules/file-system";
import { StudentExamSheet, ExamQueRecord, StudentPracExamSheet } from "./ui.module";

@Injectable({
    providedIn: "root"
})
export class UIServices {

    documents = knownFolders.documents();
    folder: Folder;
    file: File;
    examSheet: { queId: string, que: string, ans: string };

    studentExamRecord: Array<StudentExamSheet> = [];
    studentPracExamRecord: Array<StudentPracExamSheet> = [];
    private _drawerState = new BehaviorSubject<void>(null);
    private _rootVCRef: ViewContainerRef;

    get drawerState() {
        return this._drawerState.asObservable();
    }

    toggleDrawer() {
        this._drawerState.next();
    }

    getCategoriesIcons() {
        return {
            home: "\ue902",
            transport: "\uebbd",
            communication: "\uea1c",
            hotel: "\uecdc",
            restaurant: "\uebbc",
            cam: "\ue030"
        };
    }

    readFiles(folderName, fileName) {
        this.folder = this.documents.getFolder(folderName);
        this.file = this.folder.getFile(fileName);

        return new Promise((resolve) => {
            this.file.readText().then((result) => {
                return resolve({ code: 200, data: result });
            }).catch((err) => {
                console.log(err);

                return resolve({ code: 400, data: err });
            });
        });
    }

    setRootVCRef(vcRef: ViewContainerRef) {
        this._rootVCRef = vcRef;
    }

    getRootVCRef() {
        return this._rootVCRef;
    }

    writeFiles(folderName, fileName, fileContent) {

        this.folder = this.documents.getFolder(folderName || "proximo");
        this.file = this.folder.getFile((fileName) + ".json");

        return new Promise((resolve) => {
            this.file.writeText(fileContent).then((result) => {
                return resolve({ code: 200, data: result });
            }).catch((err) => {
                console.log(err);

                return resolve({ code: 400, data: err });
            });
        });

    }

    addStdExamQue(queSet: ExamQueRecord, sInfo: StudentExamSheet) {

        return new Promise((resolve) => {
            if (this.studentExamRecord.length) {
                if (!this.studentExamRecord[0].queRecord.length) {

                    this.readFiles("proximo", `${sInfo.StudentUserIdId}.json`)
                        .then((res: any) => {
                            if (res) {
                                if (res.code === 200) {
                                    if (res.data) {
                                        const existingContent = JSON.parse(res.data);
                                        this.studentExamRecord = existingContent;
                                        this.addStdExamQueSecond(queSet, sInfo).then(() => {
                                            return resolve();
                                        });
                                    }

                                }
                            }
                        });

                } else {
                    this.addStdExamQueSecond(queSet, sInfo).then(() => {
                        return resolve();
                    });
                }
            } else {
                this.addStdExamQueSecond(queSet, sInfo).then(() => {
                    return resolve();
                });
            }
        });
    }

    addStdExamQueSecond(queSet: ExamQueRecord, sInfo: StudentExamSheet) {
        return new Promise((resolve) => {
            let isSrecordUpdated = true;
            if (this.studentExamRecord.length) {
                this.studentExamRecord.forEach((sRecord) => {
                    if (sRecord.StudentUserIdId === sInfo.StudentUserIdId) {
                        if (sRecord.queRecord.length) {
                            sRecord.queRecord.forEach((examRecord) => {
                                if (examRecord.queId === queSet.queId) {
                                    // tslint:disable-next-line: max-line-length
                                    sRecord.queRecord[sRecord.queRecord.findIndex((element) => element.queId === queSet.queId)] = queSet;
                                    isSrecordUpdated = false;
                                }
                            });
                        } else {
                            sInfo.queRecord.push(queSet);
                            this.studentExamRecord.push(sInfo);
                        }
                    }
                    if (isSrecordUpdated) {
                        sRecord.queRecord.push(queSet);
                    }
                });
            } else {
                sInfo.queRecord.push(queSet);
                this.studentExamRecord.push(sInfo);
            }

            this.writeFiles("proximo", sInfo.StudentUserIdId, JSON.stringify(this.studentExamRecord)).then((res) => {

                return resolve();
            });
        });
    }

    readStdExamQue(queNumber) {
        return new Promise((resolve) => {
            if (this.studentExamRecord.length) {

                if (!this.studentExamRecord[0].queRecord.length) {
                    this.readFiles("proximo", `${this.studentExamRecord[0].StudentUserIdId}.json`)
                        .then((res: any) => {
                            if (res) {
                                if (res.code === 200) {
                                    if (res.data) {
                                        const studentRecord = JSON.parse(res.data);
                                        if (studentRecord.length) {
                                            studentRecord.forEach((sRecord) => {
                                                if (sRecord.queRecord.length) {
                                                    sRecord.queRecord.forEach((examRecord) => {
                                                        if (examRecord.queId === queNumber) {
                                                            return resolve(examRecord);
                                                        }
                                                    });
                                                } else {
                                                    return resolve(false);
                                                }
                                            });

                                        } else {
                                            return resolve(false);
                                        }
                                    }
                                }
                            }
                        });
                } else {

                    if (this.studentExamRecord[0].queRecord.length) {
                        this.studentExamRecord.forEach((sRecord) => {
                            if (sRecord.queRecord.length) {
                                sRecord.queRecord.forEach((examRecord) => {
                                    if (examRecord.queId === queNumber) {
                                        return resolve(examRecord);
                                    }
                                });
                            } else {
                                return resolve(false);
                            }
                        });
                    }
                }
            }
        });
    }

    saveStdExamQue(fileName: string) {
        return new Promise((resolve) => {
            this.readFiles("proximo", `${this.studentExamRecord[0].StudentUserIdId}.json`)
                .then((res: any) => {
                    if (res) {
                        if (res.code === 200) {
                            if (res.data) {
                                this.writeFiles("proximo", fileName, res.data).then((res) => {
                                    return resolve(res);
                                });
                            }
                        }
                    }
                });
        });
    }

    /*Practical examsheet*/

    addPracExamQue(queSet: ExamQueRecord, sInfo: StudentPracExamSheet) {

        return new Promise((resolve) => {
            if (this.studentPracExamRecord.length) {
                if (!this.studentPracExamRecord[0].queRecord.length) {

                    this.readFiles("proximo", `${sInfo.StudentUserIdId}.json`)
                        .then((res: any) => {
                            if (res) {
                                if (res.code === 200) {
                                    if (res.data) {
                                        const existingContent = JSON.parse(res.data);
                                        this.studentPracExamRecord = existingContent;
                                        this.addPracExamQueSecond(queSet, sInfo).then(() => {
                                            return resolve();
                                        });
                                    }

                                }
                            }
                        });

                } else {
                    this.addPracExamQueSecond(queSet, sInfo).then(() => {
                        return resolve();
                    });
                }
            } else {
                this.addPracExamQueSecond(queSet, sInfo).then(() => {
                    return resolve();
                });
            }
        });
    }

    addPracExamQueSecond(queSet: ExamQueRecord, sInfo: StudentPracExamSheet) {
        return new Promise((resolve) => {
            let isSrecordUpdated = true;
            if (this.studentPracExamRecord.length) {
                this.studentPracExamRecord.forEach((sRecord) => {
                    if (sRecord.StudentUserIdId === sInfo.StudentUserIdId) {
                        if (sRecord.queRecord.length) {
                            sRecord.queRecord.forEach((examRecord) => {
                                if (examRecord.queId === queSet.queId) {
                                    // tslint:disable-next-line: max-line-length
                                    sRecord.queRecord[sRecord.queRecord.findIndex((element) => element.queId === queSet.queId)] = queSet;
                                    isSrecordUpdated = false;
                                }
                            });
                        } else {
                            sInfo.queRecord.push(queSet);
                            this.studentPracExamRecord.push(sInfo);
                        }
                    }
                    if (isSrecordUpdated) {
                        sRecord.queRecord.push(queSet);
                    }
                });
            } else {
                sInfo.queRecord.push(queSet);
                this.studentPracExamRecord.push(sInfo);
            }

            this.writeFiles("proximo", sInfo.StudentUserIdId, JSON.stringify(this.studentExamRecord)).then((res) => {

                return resolve();
            });
        });
    }

    readPracExamQue(queNumber) {
        return new Promise((resolve) => {
            if (this.studentPracExamRecord.length) {

                if (!this.studentPracExamRecord[0].queRecord.length) {
                    this.readFiles("proximo", `${this.studentPracExamRecord[0].StudentUserIdId}.json`)
                        .then((res: any) => {
                            if (res) {
                                if (res.code === 200) {
                                    if (res.data) {
                                        const studentRecord = JSON.parse(res.data);
                                        if (studentRecord.length) {
                                            studentRecord.forEach((sRecord) => {
                                                if (sRecord.queRecord.length) {
                                                    sRecord.queRecord.forEach((examRecord) => {
                                                        if (examRecord.queId === queNumber) {
                                                            return resolve(examRecord);
                                                        }
                                                    });
                                                } else {
                                                    return resolve(false);
                                                }
                                            });

                                        } else {
                                            return resolve(false);
                                        }
                                    }
                                }
                            }
                        });
                } else {

                    if (this.studentPracExamRecord[0].queRecord.length) {
                        this.studentPracExamRecord.forEach((sRecord) => {
                            if (sRecord.queRecord.length) {
                                sRecord.queRecord.forEach((examRecord) => {
                                    if (examRecord.queId === queNumber) {
                                        return resolve(examRecord);
                                    }
                                });
                            } else {
                                return resolve(false);
                            }
                        });
                    }
                }
            }
        });
    }

    savePracExamQue(fileName: string) {
        return new Promise((resolve) => {
            this.readFiles("proximo", `${this.studentPracExamRecord[0].StudentUserIdId}.json`)
                .then((res: any) => {
                    if (res) {
                        if (res.code === 200) {
                            if (res.data) {
                                this.writeFiles("proximo", fileName, res.data).then((res) => {
                                    return resolve(res);
                                });
                            }
                        }
                    }
                });
        });
    }
}
