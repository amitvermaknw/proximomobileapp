
export interface ExamQueRecord {
    queId: number;
    que: string;
    ans: string;
}

export interface StudentExamSheet {
    StudentUserIdId: string;
    StudentBatchId: string;
    TestTheoryId: string;
    TestPracticalId: string;
    StudentName: string;
    StudentBatchName: string;
    Username: string;
    queRecord: Array<ExamQueRecord>;
}

export interface StudentPracExamSheet {
    StudentUserIdId: string;
    StudentBatchId: string;
    TestPracId: string;
    TestPracticalId: string;
    StudentName: string;
    StudentBatchName: string;
    Username: string;
    queRecord: Array<ExamQueRecord>;
}
