import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { UIServices } from "../ui.services";
import { RouterExtensions } from "nativescript-angular";

@Component({
    moduleId: module.id,
    selector: "ns-bottom-menu",
    templateUrl: "./bottom-menu.component.html",
    styleUrls: ["./bottom-menu.component.android.scss"]
})
export class BottomMenuComponent implements OnInit {

    constructor(
        private _uiServices: UIServices,
        private router: RouterExtensions
    ) { }

    ngOnInit() {
    }

    settingTab() {
        this.router.navigate([`/setting`]);
    }

    logoutTab() {
        this.router.navigate([`/`], { clearHistory: true });
    }

}
