import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { BottomMenuComponent } from "./bottom-menu.component";

@NgModule({
    declarations: [
        BottomMenuComponent
    ],
    imports: [
        NativeScriptCommonModule
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    exports: [
        BottomMenuComponent
    ]
})
export class BottomMenuModule { }
