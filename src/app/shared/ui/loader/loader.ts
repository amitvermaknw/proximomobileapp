import * as application from "tns-core-modules/application";
import { isIOS, isAndroid } from 'tns-core-modules/platform';
import { BehaviorSubject } from "rxjs";

declare var android;

let loaderView;

export let _uploadFileStatus = new BehaviorSubject<string>(null);

export function showLoader(message: string = 'Loading...') {
    if (loaderView) {
        return;
    }

    if (isAndroid) {
        loaderView = android.app.ProgressDialog.show(application.android.foregroundActivity, '', message);
    }
}

export function hideLoader() {
    if (!loaderView) {
        return;
    }

    if (isAndroid) {
        loaderView.dismiss();
    }

    loaderView = null;
}
