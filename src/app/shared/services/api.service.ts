import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Subject, Observable } from "rxjs";
import { take, tap } from "rxjs/operators";

import { getConnectionType } from "tns-core-modules/connectivity";
import * as connectivityModule from "tns-core-modules/connectivity";
import { UIServices } from "../ui/ui.services";


@Injectable({
    providedIn: "root"
})
export class ApiService {

    batchDetail = new Subject<object>();
    connectionType: string;

    constructor(
        private http: HttpClient,
        private _uiService: UIServices
    ) {
    }

    checkAuth(userName, password) {

        const connType = this.checkConnectionType();

        if (connType === 'mobile' || connType === 'wifi') {
            return this.http
                .get("https://assessment.proximoeducation.com/webserviceandroid.asmx/LoginUserAssessor?Username=tulsiram&Password=12345")
                .pipe(
                    tap(resData => {
                        if (resData) {
                            return resData;
                        }
                    })
                );
        } else {

            return new Observable((observe) => {
                this._uiService.readFiles("proximo", "credential.json").then((res: any) => {
                    if (res.code === 200) {
                        const loginData = JSON.parse(res.data);

                        if (loginData.username === userName && loginData.password === password) {

                            this._uiService.readFiles("proximo", "logondetails.json").then((logondetail: any) => {

                                if (logondetail.code === 200 && logondetail.data) {
                                    const logon = JSON.parse(logondetail.data);

                                    return observe.next(logon);
                                }
                            });

                        } else {
                            return observe.next(false);
                        }

                    } else {
                        return observe.next(false);
                    }
                });
            });

        }
    }

    fetchBatchList(ID: number) {
        const connType = this.checkConnectionType();

        if (connType === 'mobile' || connType === 'wifi') {
            return this.http.get(`https://assessment.proximoeducation.com/webserviceandroid.asmx/LoadAssessorBatch?AssessorId=${ID}`)
            .pipe(
                tap(resData => {
                    if (resData) {
                        return resData;
                    }
                })
            );
        } else {
            return new Observable((observe) => {
                this._uiService.readFiles("proximo", "batchlist.json").then((res: any) => {
                    if (res.code === 200 && res.data) {
                        const batchList = JSON.parse(res.data);

                        return observe.next(batchList);
                    } else {
                        return observe.next(false);
                    }
                });
            });
        }

    }

    fetchStudentList(ID: number) {
        const connType = this.checkConnectionType();

        if (connType === 'mobile' || connType === 'wifi') {
            return this.http.get(`https://assessment.proximoeducation.com/webserviceandroid.asmx/DownloadBatchData?BatchId=${ID}`,
            { responseType: "text", observe: 'response' })
            .pipe(
                tap(resData => {
                    if (resData) {
                        return resData;
                    }
                })
            );
        } else {
            return new Observable((observe) => {
                return observe.next(false);
            });
        }

    }

    checkConnectionType() {
        const type = getConnectionType();

        switch (type) {
            case connectivityModule.connectionType.none:
                this.connectionType = "none";
                break;
            case connectivityModule.connectionType.wifi:
                this.connectionType = "wifi";
                break;
            case connectivityModule.connectionType.mobile:
                this.connectionType = "mobile";
                break;
            case connectivityModule.connectionType.bluetooth:
                this.connectionType = "bluetooth";
                break;
            default:
                break;
        }

        return this.connectionType;
    }

}
