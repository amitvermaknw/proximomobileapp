import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { ActionBarModule } from "../shared/ui/action-bar/action-bar.module";
import { BottomMenuModule } from "../shared/ui/bottom-menu/bottom-menu.module";
import { SettingComponent } from "./setting.component";

import { SettingRoutingModule } from "./setting-routing.module";

@NgModule({
  declarations: [SettingComponent],
  imports: [
    NativeScriptCommonModule,
    NativeScriptUIListViewModule,
    ActionBarModule,
    SettingRoutingModule,
    BottomMenuModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class SettingModule { }
