import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { NativeScriptRouterModule } from 'nativescript-angular';
import { Routes } from "@angular/router";

import { SettingComponent } from "./setting.component";

const routes: Routes = [
    { path: "", component: SettingComponent }
];

@NgModule({
  declarations: [
  ],
  imports: [
    NativeScriptCommonModule,
    NativeScriptRouterModule.forChild(routes)
  ],
  exports: [NativeScriptRouterModule],
  schemas: [NO_ERRORS_SCHEMA]
})
export class SettingRoutingModule { }
