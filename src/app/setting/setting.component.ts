import { Component, OnInit, OnDestroy, NgZone } from "@angular/core";
import { knownFolders, File, Folder, path } from "tns-core-modules/file-system";
import { alert } from "tns-core-modules/ui/dialogs";
import * as bgHttp from "nativescript-background-http";
import * as Toast from "nativescript-toast";
import * as uiHelper from "../shared/ui/loader/loader";

import { setNumber, getNumber } from "tns-core-modules/application-settings";

@Component({
    moduleId: module.id,
    selector: "ns-setting",
    templateUrl: "./setting.component.html",
    styleUrls: ["./setting.component.android.scss"]
})
export class SettingComponent implements OnInit, OnDestroy {

    documents = knownFolders.documents();
    folderForBatchImg: Folder;
    folderForPracticalImg: Folder;
    folderForVideo: Folder;
    folder: Folder;
    folderEntities: Array<string> = [];
    fileEntitiesList: Array<{ name: string, path: string }> = [];
    processing: boolean = true;

    tasks: Array<bgHttp.Task> = [];
    events: Array<{ eventTitle: string, eventData: any }> = [];
    private file: string;
    private session: any;

    constructor(
        private ngZone: NgZone
    ) {
        uiHelper._uploadFileStatus.subscribe(() => {
            this.loadAllFiles();
        });
    }

    ngOnInit() {
        this.processing = false;
        this.folderEntities = [];
        this.loadAllFiles();
    }

    loadAllFiles() {
        uiHelper.showLoader();

        this.ngZone.run(() => {
            this.folderEntities = [];
            this.loadProximoFiles();
            this.loadBatchImageFiles();
            this.loadPracticalImgFiles();
            this.loadVideoFiles();
        });

        setTimeout(() => {
            uiHelper.hideLoader();
        }, 2000);
    }

    loadProximoFiles() {
        this.folderForBatchImg = this.documents.getFolder("proximo");

        this.folderForBatchImg.getEntities()
            .then((entities) => {
                entities.forEach((entity) => {
                    const fileName = (entity.name).split("_");
                    if (fileName[0] === "prc" || fileName[0] === "thy") {
                        this.folderEntities.push(
                            entity.name
                        );
                    }
                });

            }).catch((err) => {

                alert({
                    title: "Alert",
                    message: "There was some error while submitting the exam. Please try after sometime." + err.message,
                    okButtonText: "Okay"
                }).then(() => {
                    console.log("The user closed the alert.");
                });
            });
    }

    loadBatchImageFiles() {
        this.folderForBatchImg = this.documents.getFolder("proximo");

        this.folderForBatchImg = this.folderForBatchImg.getFolder("BatchImages");
        this.folderForBatchImg.getEntities()
            .then((entities) => {
                entities.forEach((entity) => {
                    const fileName = (entity.name).split(".");
                    if (fileName[1]) {
                        if (fileName[1].toLocaleLowerCase() === "jpg"
                            || fileName[1].toLocaleLowerCase() === "jpeg"
                            || fileName[1].toLocaleLowerCase() === "png"
                            || fileName[1].toLocaleLowerCase() === "json") {
                            this.folderEntities.push(
                                entity.name
                            );
                        }
                    }
                });

            }).catch((err) => {

                alert({
                    title: "Alert",
                    message: "There was some error while submitting the exam. Please try after sometime." + err.message,
                    okButtonText: "Okay"
                }).then(() => {
                    console.log("The user closed the alert.");
                });
            });
    }

    loadPracticalImgFiles() {
        this.folderForPracticalImg = this.documents.getFolder("proximo");

        this.folderForPracticalImg = this.folderForPracticalImg.getFolder("PracticalStdImages");
        this.folderForPracticalImg.getEntities()
            .then((entities) => {
                entities.forEach((entity) => {
                    this.folderEntities.push(
                        entity.name
                    );
                });

            }).catch((err) => {

                alert({
                    title: "Alert",
                    message: "There was some error while submitting the exam. Please try after sometime." + err.message,
                    okButtonText: "Okay"
                }).then(() => {
                    console.log("The user closed the alert.");
                });
            });
    }

    loadVideoFiles() {
        this.folderForVideo = Folder.fromPath("/storage/emulated/0/DCIM/Camera/proximo");

        this.folderForVideo.getEntities()
            .then((entities) => {
                entities.forEach((entity) => {
                    this.folderEntities.push(
                        entity.name
                    );
                });

            }).catch((err) => {

                alert({
                    title: "Alert",
                    message: "There was some error while submitting the exam. Please try after sometime." + err.message,
                    okButtonText: "Okay"
                }).then(() => {
                    console.log("The user closed the alert.");
                });
            });
    }

    uploadingImg() {

        if (this.folderEntities.length) {
            uiHelper.showLoader();

            this.folderEntities.forEach((fileName) => {

                const fileExt = (fileName).split(".");

                if (fileExt[1]) {
                    if (fileExt[1].toLocaleLowerCase() === "mp4"
                        || fileExt[1].toLocaleLowerCase() === "mp3"
                        || fileExt[1].toLocaleLowerCase() === "3gp"
                        || fileExt[1].toLocaleLowerCase() === "mkv"
                        || fileExt[1].toLocaleLowerCase() === "wav"
                    ) {
                        this.folder = Folder.fromPath("/storage/emulated/0/DCIM/Camera/proximo");

                        const videoFilePath = path.join(this.folder.path, fileName);
                        const videoFileExist = File.exists(videoFilePath);

                        if (videoFileExist) {
                            this.file = path.normalize(this.folder.getFile((fileName)).path);
                        }
                    } else {
                        this.folder = this.documents.getFolder("proximo");
                        const filePath = path.join(this.folder.path, fileName);
                        const fileExist = File.exists(filePath);

                        if (fileExist) {
                            this.file = path.normalize(this.folder.getFile((fileName)).path);
                        }

                        this.folder = this.folder.getFolder("BatchImages");
                        const imageFilePath = path.join(this.folder.path, fileName);
                        const imageFileExist = File.exists(imageFilePath);

                        if (imageFileExist) {
                            this.file = path.normalize(this.folder.getFile((fileName)).path);
                        }

                        this.folder = this.documents.getFolder("proximo");
                        this.folder = this.folder.getFolder("PracticalStdImages");
                        const pracimageFilePath = path.join(this.folder.path, fileName);
                        const pracimageFileExist = File.exists(pracimageFilePath);

                        if (pracimageFileExist) {
                            this.file = path.normalize(this.folder.getFile((fileName)).path);
                        }
                    }
                }

                if (this.file) {
                    this.fileEntitiesList.push({ name: fileName, path: this.file });
                }

            });

            this.sendFile().then((res) => {
                if (res) {
                    // this.processing = false;
                }
            });

        } else {
            alert({
                title: "Alert",
                message: "There is no file to upload.",
                okButtonText: "Okay"
            }).then(() => {
                console.log("The user closed the alert.");
            });
        }
    }

    sendFile() {
        try {
            return new Promise((resolve) => {
                if (this.fileEntitiesList.length) {

                    setNumber("fileToUpload", this.fileEntitiesList.length);
                    setNumber("uploadedFile", 1);

                    this.fileEntitiesList.forEach((fileObj) => {

                        this.session = bgHttp.session(fileObj.name + new Date());
                        const request = {
                            url: "https://assessment.proximoeducation.com/WebServiceAndroid.asmx/UploadExamResultFile",
                            method: "POST",
                            headers: {
                                "Content-Type": "application/octet-stream",
                                "file-Name": fileObj.name,
                                uid: 30
                            },
                            description: "{ 'uploading': '" + fileObj.name + "' }",
                            androidDisplayNotificationProgress: true,
                            androidNotificationTitle: "Proximo exam file upload",
                            androidMaxRetries: 3,
                            androidAutoDeleteAfterUpload: true
                        };

                        const params = [{ name: fileObj.name, filename: fileObj.path, mimeType: "image/jpeg" }];
                        let task: bgHttp.Task;

                        task = this.session.multipartUpload(params, request);
                        // task = this.session.uploadFile(this.file, request);

                        task.on("progress", this.progressHandler);
                        task.on("error", this.errorHandler);
                        task.on("responded", this.respondedHandler);
                        task.on("complete", this.completeHandler);

                    });
                }
            });

        } catch (e) {
            console.log("inside catch while uploading.");
            uiHelper.hideLoader();
        }
    }

    progressHandler(e) {
        console.log("uploaded = " + e.currentBytes + " / " + e.totalBytes);
    }

    errorHandler(e) {
        console.log("received error = " + e.responseCode + " code.");
        alert({
            title: "Alert",
            message: "There was some error while submitting the exam. Please try after sometime." + e.responseCode,
            okButtonText: "Okay"
        }).then(() => {
            this.processing = false;
            console.log("The user closed the alert.");
        });

        uiHelper.hideLoader();
    }

    respondedHandler(e) {
        if (e.data != "") {
            const response = JSON.parse(e.data);
            if (response[0].Status === "200") {
                Toast.makeText(response[0].Message, "long").show();
            }
        }
    }

    completeHandler(e) {

        let uploadedFile = getNumber("uploadedFile");
        const fileToUpload = getNumber("fileToUpload");

        if (fileToUpload === uploadedFile) {
            uiHelper._uploadFileStatus.next("reset");

            uiHelper.hideLoader();
        } else {
            uploadedFile++;
            setNumber("uploadedFile", uploadedFile);
        }
    }

    ngOnDestroy() {
        uiHelper.hideLoader();
    }
}
