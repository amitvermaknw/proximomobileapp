import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular/side-drawer-directives";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { TNSCheckBoxModule } from "@nstudio/nativescript-checkbox/angular";
import { ReactiveFormsModule } from "@angular/forms";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { AuthModule } from "./auth/auth.module";
import { QuestionSheetComponent, FormatTimePipe } from "./exam/theory-exam/exam-grid/question-sheet/question-sheet.component";
import { AppsettingComponent } from "./shared/appsetting/appsetting.component";

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptModule,
        AppRoutingModule,
        NativeScriptFormsModule,
        NativeScriptUISideDrawerModule,
        NativeScriptHttpClientModule,
        TNSCheckBoxModule,
        ReactiveFormsModule,
        AuthModule
    ],
    declarations: [
        AppComponent,
        QuestionSheetComponent,
        FormatTimePipe,
        AppsettingComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    entryComponents: [
        QuestionSheetComponent
    ]
})
export class AppModule { }
