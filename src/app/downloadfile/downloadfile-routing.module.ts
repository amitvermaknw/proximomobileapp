import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { DownloadfileComponent } from "./downloadfile.component";

const routes: Routes = [
    { path: "", component: DownloadfileComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class DownloadfileRoutingModule { }
