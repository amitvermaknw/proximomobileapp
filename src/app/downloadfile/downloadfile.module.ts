import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { DownloadfileRoutingModule } from "./downloadfile-routing.module";
import { ActionBarModule } from "../shared/ui/action-bar/action-bar.module";
import { BottomMenuModule } from "../shared/ui/bottom-menu/bottom-menu.module";

import { DownloadfileComponent } from "./downloadfile.component";

@NgModule({
    declarations: [
        DownloadfileComponent
    ],
    imports: [
        NativeScriptCommonModule,
        DownloadfileRoutingModule,
        ActionBarModule,
        BottomMenuModule
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class DownloadfileModule { }
