import { Component, OnInit } from '@angular/core';
import { getString } from "tns-core-modules/application-settings";
import { RouterExtensions } from 'nativescript-angular';
import { ApiService } from '../shared/services/api.service';
import { UIServices } from '../shared/ui/ui.services';
import { alert, prompt } from "tns-core-modules/ui/dialogs";
import * as uiHelper from "../shared/ui/loader/loader";

@Component({
    moduleId: module.id,
    selector: 'ns-downloadfile',
    templateUrl: './downloadfile.component.html',
    styleUrls: ['./downloadfile.component.android.scss']
})
export class DownloadfileComponent implements OnInit {

    processing: boolean = false;

    constructor(
        private _router: RouterExtensions,
        private _apiService: ApiService,
        private _uiService: UIServices
    ) { }

    ngOnInit() {
    }

    downloadQue() {

        this.processing = true;
        uiHelper.showLoader();
        const batchId = +getString("batchid");

        this._apiService.fetchStudentList(batchId).subscribe((studentList: any) => {

            if (studentList) {
                studentList = JSON.parse(studentList.body);
                const sList = studentList[0];
                if (sList.Status === "200") {
                    this._uiService.writeFiles("proximo", "studentlist", JSON.stringify(sList)).then((res: any) => {

                        if (res.code === 200) {
                            alert({
                                title: "Alert",
                                message: "Questions has been downloaded. Please click on next button to go further.",
                                okButtonText: "Okay"
                            }).then(() => {
                                // console.log("The user closed the alert.");
                            });
                        } else {
                            alert({
                                title: "Alert",
                                message: "There was some error while downloading the question. Please try after sometime.",
                                okButtonText: "Okay"
                            }).then(() => {
                                // console.log("The user closed the alert.");
                            });
                        }
                        this.processing = false;
                        uiHelper.hideLoader();
                    });

                } else {
                    this.processing = false;
                    uiHelper.hideLoader();

                    alert({
                        title: "Alert",
                        message: "There was some error while downloading the question. Please try after sometime.",
                        okButtonText: "Okay"
                    }).then(() => {});
                }
            } else {
                this.processing = false;
                uiHelper.hideLoader();

                alert({
                    title: "Alert",
                    message: "Internet is not available. Please connect through WiFi.",
                    okButtonText: "Okay"
                }).then(() => {});
            }
        });
    }

    nextScreen() {
        uiHelper.showLoader();
        setTimeout(() => {
            const batchId = getString("batchid");
            this._router.navigate([`/batchevidence/${batchId}`]);
        }, 200);

    }

}
