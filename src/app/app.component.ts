import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit,
    ChangeDetectorRef, ViewContainerRef } from "@angular/core";
import { isAndroid } from "tns-core-modules/platform";
import { UIServices } from "./shared/ui/ui.services";
import { Subscription } from "rxjs";
import { RadSideDrawerComponent } from "nativescript-ui-sidedrawer/angular/side-drawer-directives";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { CameraPlus } from "@nstudio/nativescript-camera-plus";
import { registerElement } from "nativescript-angular/element-registry";

registerElement("CameraPlus", () => <any>CameraPlus);

@Component({
    selector: "ns-app",
    moduleId: module.id,
    templateUrl: "app.component.html",
    styleUrls: ["./app.component.android.scss"]
})
export class AppComponent implements OnInit, OnDestroy, AfterViewInit {
    @ViewChild(RadSideDrawerComponent, { static: true }) drawerComponet: RadSideDrawerComponent;

    private drawerState: Subscription;
    private drawer: RadSideDrawer;
    private _uiService: UIServices;
    private vcRef: ViewContainerRef;

    constructor(
        private _uiServices: UIServices,
        private changeDetectRaf: ChangeDetectorRef
    ) { }

    ngOnInit() {
        this.drawerState = this._uiServices.drawerState.subscribe(() => {
            if (this.drawer) {
                this.drawer.toggleDrawerState();
            }
        });
        // this._uiService.setRootVCRef(this.vcRef);
    }

    getIconSource(icon: string): string {
        const iconPrefix = isAndroid ? "res://" : "res://tabIcons/";

        return iconPrefix + icon;
    }

    ngOnDestroy() {
        if (this.drawerState) {
            this.drawerState.unsubscribe();
        }
    }

    ngAfterViewInit() {
        // this.drawer = this.drawerComponet.sideDrawer;
        this.changeDetectRaf.detectChanges();
    }

    onLogout() {
        this._uiServices.toggleDrawer();
    }
}
